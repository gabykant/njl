$(document).ready(function(){

    $('#add_education').click(function(e){
        e.preventDefault();
        var inp = $('#box');
        var i = $('input').length + 1;

        $('<div id="box' + i +'">' +
              '<div class="row">' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +
                    '</div>' +
                    '<div class="col-md-9">' +
                        '<input type="text" name="school_name[]" id="" class="form-control Form_element" placeholder="School">' +
                    '</div>' +

                '</div>' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +

                    '</div>' +
                    '<div class="col-md-8">' +
                        '<input type="text" name="program[]" id="" class="form-control Form_element" placeholder="Program">' +
                    '</div>' +
                '</div>' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +

                    '</div>' +
                    '<div class="col-md-4">' +
                        '<input type="text" name="start_date[]" id="" class="form-control Form_element" placeholder="Start">' +
                    '</div>' +
                    '<div class="col-md-4">' +
                        '<input type="text" name="end_date[]" id="" class="form-control Form_element" placeholder="End">' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>').appendTo(inp);
        i++;
    });

    $('body').on('click','#remove',function(){
        $(this).parent('div').remove();
    });
    
    
    $('#jsAddSkill').on('click', function(e){
        e.preventDefault();
        var experience = $('#lb_experience').val();
        var _token = $('input[name="_token"]').val();
        $.post( "/skills/buildArray", { 
            '_token' : _token, 
            'lb_skill_name': $('#lb_skill_name').val(), 
            'lb_experience': $('#lb_experience').val(), 
            'lb_skillsperiod_id': $('#lb_skillsperiod_id').val()
        }).done(function(data) {
            $("#skills_list").html("");
            var d = jQuery.parseJSON(data);
            var lineIn="";
            $.each(d, function(key, value){
                var val = jQuery.parseJSON(value);
                console.log(val.lb_skill_id);
                lineIn +="<tr>";
                lineIn +="<td style='width: 200px'>" + val.lb_skill_name + "</td>"; 
                lineIn +="<td style='width: 200px'>" + val.lb_experience + "</td>"; 
                lineIn +="<td style='width: 200px'>" + val.lb_skillsperiod_id + "</td>"; 
                lineIn +="<td><a onclick='removeFromList(" + key + "); return false;'>remove</a></td></tr>";
            });
            $("#skills_list").append(lineIn);
            
        }).fail(function(){
            console.log("An error occurs");
        });
    });
    
    $('#lb_country_id').change(function(){
        var country = $(this).val();
        $("#lb_state_id").html("");
        $.get( '/states/list' , { lb_country_id: country } , function(htmlCode){ 
            var states_list = jQuery.parseJSON(htmlCode);
            var options_states="";
            $.each(states_list, function(k,v){
                options_states +="<option value='"+ k +"'>" + v + "</option>";
            });
            console.log(options_states);
            $("#lb_state_id").append(options_states);
        });
    });    
    
    $('#lb_state_id').change(function(){
        var state_id = $(this).val();
        $("#lb_city_id").html("");
        $.get( '/cities/list', { lb_state_id: state_id}, function(optionCities){
            var cities_list = jQuery.parseJSON(optionCities);
            var options_cities = "";
            $.each(cities_list, function(k,v){
                options_cities +="<option value='" + k + "'>" + v + "</option>";    
            });
            $("#lb_city_id").append(options_cities);
        });
    });
    
    $('#jsAddSkillByUser').on('click', function(e){
        e.preventDefault();
        var new_skill = $("#new_skill").val();
        var _token = $('input[name="_token"]').val();
        $.post('/customer/skills/all', { 'skills_name': new_skill, '_token': _token }, function(data){
            console.log(data);
            //$("#close_me2").trigger('click');
            $("#close_me2").modal('hide');
        });
    });
    
    $.get('/list/jobs/search/', {}, function(e){
        
    });

    $("#location_offline").toggle();
    $("#location").change(function(e){
        var toggle = $("#location").val();
        if(toggle == 'online'){
            $("#location_online").toggle();
            $("#location_offline").toggle();
        }else{
            $("#location_offline").toggle();
            $("#location_online").toggle();
        }
    });

    $("#graduated-yes").toggle();
    $("input[name=lb_graduated]:radio").change(function(e){
        var click=$("#lb_graduated").val();
        if(click == 'no'){
            $("#graduated-yes").toggle();
            $("#graduated-no").toggle();
        }else{
            $("#graduated-no").toggle();
            $("#graduated-yes").toggle();
        }
    })
});
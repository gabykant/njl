-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: njl
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` VALUES (1,0,1,'Index','fa-bar-chart','/',NULL,NULL),(2,0,2,'Admin','fa-tasks','',NULL,NULL),(3,2,3,'Users','fa-users','auth/users',NULL,NULL),(4,2,4,'Roles','fa-user','auth/roles',NULL,NULL),(5,2,5,'Permission','fa-user','auth/permissions',NULL,NULL),(6,2,6,'Menu','fa-bars','auth/menu',NULL,NULL),(7,2,7,'Operation log','fa-history','auth/logs',NULL,NULL),(8,0,8,'Helpers','fa-gears','',NULL,NULL),(9,8,9,'Scaffold','fa-keyboard-o','helpers/scaffold',NULL,NULL),(10,8,10,'Database terminal','fa-database','helpers/terminal/database',NULL,NULL),(11,8,11,'Laravel artisan','fa-terminal','helpers/terminal/artisan',NULL,NULL),(12,0,12,'Post a job','fa-graduation-cap','new/post','2017-06-12 04:41:56','2017-06-12 04:42:17');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_operation_log`
--

LOCK TABLES `admin_operation_log` WRITE;
/*!40000 ALTER TABLE `admin_operation_log` DISABLE KEYS */;
INSERT INTO `admin_operation_log` VALUES (1,1,'admin','GET','192.168.10.1','[]','2017-06-12 04:29:43','2017-06-12 04:29:43'),(2,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:30:25','2017-06-12 04:30:25'),(3,1,'admin/auth/permissions','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:30:44','2017-06-12 04:30:44'),(4,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:31:49','2017-06-12 04:31:49'),(5,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:02','2017-06-12 04:32:02'),(6,1,'admin/auth/roles','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:33','2017-06-12 04:32:33'),(7,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:39','2017-06-12 04:32:39'),(8,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:52','2017-06-12 04:32:52'),(9,1,'admin/auth/permissions','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:00','2017-06-12 04:33:00'),(10,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:06','2017-06-12 04:33:06'),(11,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"candidate_permission\",\"name\":\"CANDIDATE PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:33:37','2017-06-12 04:33:37'),(12,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:33:38','2017-06-12 04:33:38'),(13,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:45','2017-06-12 04:33:45'),(14,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"recruiter_permission\",\"name\":\"RECRUITER PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:34:14','2017-06-12 04:34:14'),(15,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:34:15','2017-06-12 04:34:15'),(16,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:19','2017-06-12 04:34:19'),(17,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"employer_permission\",\"name\":\"EMPLOYER PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:34:47','2017-06-12 04:34:47'),(18,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:34:48','2017-06-12 04:34:48'),(19,1,'admin/auth/roles','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:51','2017-06-12 04:34:51'),(20,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:55','2017-06-12 04:34:55'),(21,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"candidate\",\"name\":\"CANDIDATE\",\"permissions\":[\"1\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:35:39','2017-06-12 04:35:39'),(22,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:35:40','2017-06-12 04:35:40'),(23,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:35:48','2017-06-12 04:35:48'),(24,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"recruiter\",\"name\":\"RECRUITER\",\"permissions\":[\"2\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:36:06','2017-06-12 04:36:06'),(25,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:36:06','2017-06-12 04:36:06'),(26,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:10','2017-06-12 04:36:10'),(27,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"employer\",\"name\":\"EMPLOYER\",\"permissions\":[\"3\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:36:24','2017-06-12 04:36:24'),(28,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:36:26','2017-06-12 04:36:26'),(29,1,'admin/auth/users','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:36','2017-06-12 04:36:36'),(30,1,'admin/auth/users/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:42','2017-06-12 04:36:42'),(31,1,'admin/auth/users','POST','192.168.10.1','{\"username\":\"gabykant\",\"name\":\"Kwaye Kant\",\"password\":\"computer\",\"password_confirmation\":\"computer\",\"roles\":[\"2\",null],\"permissions\":[\"1\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/users\"}','2017-06-12 04:38:57','2017-06-12 04:38:57'),(32,1,'admin/auth/users','GET','192.168.10.1','[]','2017-06-12 04:38:59','2017-06-12 04:38:59'),(33,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:39:17','2017-06-12 04:39:17'),(34,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Post a job\",\"icon\":\"fa-graduation-cap\",\"uri\":\"new\\/post\",\"roles\":[\"3\",\"4\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\"}','2017-06-12 04:41:55','2017-06-12 04:41:55'),(35,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-12 04:41:57','2017-06-12 04:41:57'),(36,1,'admin/auth/menu','POST','192.168.10.1','{\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":12}]\"}','2017-06-12 04:42:17','2017-06-12 04:42:17'),(37,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:42:18','2017-06-12 04:42:18'),(38,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:45:41','2017-06-12 04:45:41'),(39,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/menu\"}','2017-06-12 04:48:05','2017-06-12 04:48:05'),(40,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:48:07','2017-06-12 04:48:07'),(41,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\"}','2017-06-12 04:49:04','2017-06-12 04:49:04'),(42,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:49:05','2017-06-12 04:49:05'),(43,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\"}','2017-06-12 04:53:37','2017-06-12 04:53:37'),(44,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:53:38','2017-06-12 04:53:38'),(45,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:53:48','2017-06-12 04:53:48'),(46,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:54:28','2017-06-12 04:54:28'),(47,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:02','2017-06-12 04:55:02'),(48,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/login\"}','2017-06-12 04:55:36','2017-06-12 04:55:36'),(49,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:38','2017-06-12 04:55:38'),(50,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:55:45','2017-06-12 04:55:45'),(51,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:50','2017-06-12 04:55:50'),(52,1,'admin','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:55:58','2017-06-12 04:55:58'),(53,1,'admin','GET','192.168.10.1','[]','2017-06-12 09:59:31','2017-06-12 09:59:31'),(54,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 09:59:53','2017-06-12 09:59:53'),(55,1,'admin','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 10:00:02','2017-06-12 10:00:02');
/*!40000 ALTER TABLE `admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_permissions`
--

LOCK TABLES `admin_permissions` WRITE;
/*!40000 ALTER TABLE `admin_permissions` DISABLE KEYS */;
INSERT INTO `admin_permissions` VALUES (1,'CANDIDATE PERMISSION','candidate_permission','2017-06-12 04:33:38','2017-06-12 04:33:38'),(2,'RECRUITER PERMISSION','recruiter_permission','2017-06-12 04:34:14','2017-06-12 04:34:14'),(3,'EMPLOYER PERMISSION','employer_permission','2017-06-12 04:34:47','2017-06-12 04:34:47');
/*!40000 ALTER TABLE `admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_menu`
--

LOCK TABLES `admin_role_menu` WRITE;
/*!40000 ALTER TABLE `admin_role_menu` DISABLE KEYS */;
INSERT INTO `admin_role_menu` VALUES (1,2,NULL,NULL),(1,8,NULL,NULL),(3,12,NULL,NULL),(4,12,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_permissions`
--

LOCK TABLES `admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `admin_role_permissions` DISABLE KEYS */;
INSERT INTO `admin_role_permissions` VALUES (2,1,NULL,NULL),(3,2,NULL,NULL),(4,3,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_users`
--

LOCK TABLES `admin_role_users` WRITE;
/*!40000 ALTER TABLE `admin_role_users` DISABLE KEYS */;
INSERT INTO `admin_role_users` VALUES (1,1,NULL,NULL),(2,2,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_roles`
--

LOCK TABLES `admin_roles` WRITE;
/*!40000 ALTER TABLE `admin_roles` DISABLE KEYS */;
INSERT INTO `admin_roles` VALUES (1,'Administrator','administrator','2017-06-12 04:27:16','2017-06-12 04:27:16'),(2,'CANDIDATE','candidate','2017-06-12 04:35:39','2017-06-12 04:35:39'),(3,'RECRUITER','recruiter','2017-06-12 04:36:06','2017-06-12 04:36:06'),(4,'EMPLOYER','employer','2017-06-12 04:36:24','2017-06-12 04:36:24');
/*!40000 ALTER TABLE `admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_permissions`
--

LOCK TABLES `admin_user_permissions` WRITE;
/*!40000 ALTER TABLE `admin_user_permissions` DISABLE KEYS */;
INSERT INTO `admin_user_permissions` VALUES (2,1,NULL,NULL);
/*!40000 ALTER TABLE `admin_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy','Kwaye Kant','image/IMG_20170116_164506[1].jpg',NULL,'2017-06-12 04:27:16','2017-06-12 04:55:37'),(2,'gabykant','$2y$10$DdKrOBh0h6A/EXtso9mMMOut5ycPKI3LVNXUiZaFgP6JWHWJh.jsO','Kwaye Kant',NULL,NULL,'2017-06-12 04:38:58','2017-06-12 04:38:58');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2016_01_04_173148_create_admin_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_award`
--

DROP TABLE IF EXISTS `tbl_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_award` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_issuing_organization` varchar(45) DEFAULT NULL,
  `lb_date` int(11) DEFAULT NULL,
  `lb_place` varchar(45) DEFAULT NULL,
  `lb_description` text,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_award_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_award`
--

LOCK TABLES `tbl_award` WRITE;
/*!40000 ALTER TABLE `tbl_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_candidate`
--

DROP TABLE IF EXISTS `tbl_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_candidate` (
  `id` int(11) NOT NULL,
  `lb_user_id` int(11) DEFAULT NULL,
  `lb_gender_id` int(11) DEFAULT NULL COMMENT 'This is the foreign of the Gender table',
  `lb_last_name` varchar(45) DEFAULT NULL,
  `lb_first_name` varchar(45) DEFAULT NULL,
  `lb_middle_name` varchar(45) DEFAULT NULL,
  `lb_dob` int(11) DEFAULT NULL COMMENT '''The day of the birth. Stored in the INT format''',
  `lb_email` varchar(45) DEFAULT NULL,
  `lb_primary_phone` varchar(45) DEFAULT NULL,
  `lb_fax` varchar(255) DEFAULT NULL,
  `lb_facebook_url` varchar(45) DEFAULT NULL,
  `lb_twitter_url` varchar(45) DEFAULT NULL,
  `lb_linkedin_url` varchar(45) DEFAULT NULL,
  `lb_extra_activity` text,
  `lb_hobbies` text,
  `lb_photo_url` varchar(255) DEFAULT NULL,
  `lb_graduate_date` int(11) DEFAULT NULL,
  `lb_program_studying_id` int(11) DEFAULT NULL,
  `lb_degree_type_id` int(11) DEFAULT NULL,
  `lb_start_date` int(11) DEFAULT NULL,
  `lb_end_date` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_position` varchar(45) DEFAULT NULL,
  `lb_department` varchar(45) DEFAULT NULL,
  `lb_company_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_candidate_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_candidate`
--

LOCK TABLES `tbl_candidate` WRITE;
/*!40000 ALTER TABLE `tbl_candidate` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_candidate_skill`
--

DROP TABLE IF EXISTS `tbl_candidate_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_candidate_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_candidate_id` int(11) DEFAULT NULL COMMENT 'The foreign key of Tbl_candidate',
  `lb_skill_id` int(11) DEFAULT NULL COMMENT 'The foreign key of Tbl_skill',
  `lb_length_experience` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_candidate_skill`
--

LOCK TABLES `tbl_candidate_skill` WRITE;
/*!40000 ALTER TABLE `tbl_candidate_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_candidate_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_contact_information`
--

DROP TABLE IF EXISTS `tbl_contact_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contact_information` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_address_type` int(11) DEFAULT NULL COMMENT 'This can be 2 possible value : HomeAddress(0) or WorkAddress(1)',
  `lb_house_number` varchar(45) DEFAULT NULL,
  `lb_appartment_number` varchar(45) DEFAULT NULL,
  `lb_street_name` varchar(255) DEFAULT NULL,
  `lb_city` int(11) DEFAULT NULL,
  `lb_county` int(11) DEFAULT NULL,
  `lb_state` int(11) DEFAULT NULL,
  `lb_country` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contact_information`
--

LOCK TABLES `tbl_contact_information` WRITE;
/*!40000 ALTER TABLE `tbl_contact_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_contact_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_country_name` varchar(45) DEFAULT NULL,
  `lb_country_code` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`lb_country_code`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country`
--

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_day`
--

DROP TABLE IF EXISTS `tbl_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_day` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_day`
--

LOCK TABLES `tbl_day` WRITE;
/*!40000 ALTER TABLE `tbl_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_degree_type`
--

DROP TABLE IF EXISTS `tbl_degree_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_degree_type` (
  `id` int(11) NOT NULL,
  `lb_degree_name` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_degree_type_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_degree_type`
--

LOCK TABLES `tbl_degree_type` WRITE;
/*!40000 ALTER TABLE `tbl_degree_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_degree_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_education_history`
--

DROP TABLE IF EXISTS `tbl_education_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_education_history` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_address` varchar(45) DEFAULT NULL,
  `lb_attended_from` datetime DEFAULT NULL,
  `lb_attended_to` datetime DEFAULT NULL,
  `lb_is_graduated` set('0','1') DEFAULT '1' COMMENT 'To check if the concerned is graduated (0) or not (1)',
  `lb_education_type` varchar(45) DEFAULT NULL COMMENT 'A dropdown list to select from: HighSchool-College-University',
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_education_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_education_history`
--

LOCK TABLES `tbl_education_history` WRITE;
/*!40000 ALTER TABLE `tbl_education_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_education_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_employer_recruiter`
--

DROP TABLE IF EXISTS `tbl_employer_recruiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employer_recruiter` (
  `id` int(11) NOT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_city_id` int(11) DEFAULT NULL,
  `lb_state_id` int(11) DEFAULT NULL,
  `lb_email` varchar(45) DEFAULT NULL,
  `lb_phone_number` varchar(45) DEFAULT NULL,
  `lb_fax` varchar(45) DEFAULT NULL,
  `lb_facebook_url` varchar(45) DEFAULT NULL,
  `lb_twitter_url` varchar(45) DEFAULT NULL,
  `lb_linkedin_url` varchar(45) DEFAULT NULL,
  `lb_company_news` text,
  `lb_company_logo_path` varchar(45) DEFAULT NULL,
  `lb_industry_type_id` int(11) DEFAULT NULL,
  `lb_company_portrait` text,
  `lb_hiring_firstname` varchar(255) DEFAULT NULL,
  `lb_hiring_middlename` varchar(255) DEFAULT NULL,
  `lb_hiring_lastname` varchar(255) DEFAULT NULL,
  `lb_company_create_date` int(11) DEFAULT NULL,
  `lb_hiring_status` set('0','1') DEFAULT '1' COMMENT '0 for status NOWHIRING and 1 for NoVACANCY',
  `lb_position` varchar(45) DEFAULT NULL,
  `lb_department` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_employer_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_employer_recruiter`
--

LOCK TABLES `tbl_employer_recruiter` WRITE;
/*!40000 ALTER TABLE `tbl_employer_recruiter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_employer_recruiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_employment_status`
--

DROP TABLE IF EXISTS `tbl_employment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employment_status` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_group_status` varchar(45) DEFAULT NULL COMMENT 'This is a dropdown list with values CURRENTLY EMPLOYED- SCHOOLING - EMPLOYED_OR_SCHOOLING',
  `lb_address` text,
  `lb_started_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_employment_status_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_employment_status`
--

LOCK TABLES `tbl_employment_status` WRITE;
/*!40000 ALTER TABLE `tbl_employment_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_employment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_extra_activity`
--

DROP TABLE IF EXISTS `tbl_extra_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_extra_activity` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_extra_activity`
--

LOCK TABLES `tbl_extra_activity` WRITE;
/*!40000 ALTER TABLE `tbl_extra_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_extra_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_gender`
--

DROP TABLE IF EXISTS `tbl_gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_gender` (
  `id` int(11) NOT NULL,
  `lb_name_gender` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_gender`
--

LOCK TABLES `tbl_gender` WRITE;
/*!40000 ALTER TABLE `tbl_gender` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_hiring_status`
--

DROP TABLE IF EXISTS `tbl_hiring_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_hiring_status` (
  `id` int(11) NOT NULL,
  `lb_employer_id` int(11) DEFAULT NULL,
  `lb_job_category` varchar(45) DEFAULT NULL,
  `lb_position` varchar(45) DEFAULT NULL,
  `lb_department` varchar(45) DEFAULT NULL,
  `lb_location_city` varchar(45) DEFAULT NULL,
  `lb_location_state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_hiring_status`
--

LOCK TABLES `tbl_hiring_status` WRITE;
/*!40000 ALTER TABLE `tbl_hiring_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_hiring_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_industry_type`
--

DROP TABLE IF EXISTS `tbl_industry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_industry_type` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(255) DEFAULT NULL,
  `lb_is_delete` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_industry_type_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_industry_type`
--

LOCK TABLES `tbl_industry_type` WRITE;
/*!40000 ALTER TABLE `tbl_industry_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_industry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job`
--

DROP TABLE IF EXISTS `tbl_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job` (
  `id` int(11) NOT NULL,
  `lb_skill_id` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_additional_skill_id` int(11) DEFAULT NULL,
  `lb_job_category_id` int(11) DEFAULT NULL,
  `lb_title` varchar(45) DEFAULT NULL,
  `lb_education` varchar(45) DEFAULT NULL,
  `lb_job_description` text,
  `lb_recruiter_id` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_job_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job`
--

LOCK TABLES `tbl_job` WRITE;
/*!40000 ALTER TABLE `tbl_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_apply`
--

DROP TABLE IF EXISTS `tbl_job_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_apply` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_job_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_job_apply_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_apply`
--

LOCK TABLES `tbl_job_apply` WRITE;
/*!40000 ALTER TABLE `tbl_job_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_category`
--

DROP TABLE IF EXISTS `tbl_job_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_category_name` varchar(255) DEFAULT NULL,
  `lb_description` text,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_category`
--

LOCK TABLES `tbl_job_category` WRITE;
/*!40000 ALTER TABLE `tbl_job_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_type`
--

DROP TABLE IF EXISTS `tbl_job_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_type` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_job_type_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_type`
--

LOCK TABLES `tbl_job_type` WRITE;
/*!40000 ALTER TABLE `tbl_job_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_month`
--

DROP TABLE IF EXISTS `tbl_month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_month` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_month`
--

LOCK TABLES `tbl_month` WRITE;
/*!40000 ALTER TABLE `tbl_month` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_month` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_professional_organization`
--

DROP TABLE IF EXISTS `tbl_professional_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_professional_organization` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_name` varchar(255) DEFAULT NULL,
  `lb_location` text,
  `lb_participation_date` int(11) DEFAULT NULL,
  `lb_role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_professional_organization`
--

LOCK TABLES `tbl_professional_organization` WRITE;
/*!40000 ALTER TABLE `tbl_professional_organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_professional_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_professional_training`
--

DROP TABLE IF EXISTS `tbl_professional_training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_professional_training` (
  `id` int(11) NOT NULL,
  `lb_trainer` varchar(255) DEFAULT NULL,
  `lb_training_period_from` int(11) DEFAULT NULL,
  `lb_training_period_to` int(11) DEFAULT NULL,
  `lb_certificate` varchar(45) DEFAULT NULL,
  `lb_certificate_issuing_autority` varchar(45) DEFAULT NULL,
  `lb_certificate_issued_date` int(11) DEFAULT NULL,
  `lb_certificate_expiration_date` int(11) DEFAULT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_profesional_training_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_professional_training`
--

LOCK TABLES `tbl_professional_training` WRITE;
/*!40000 ALTER TABLE `tbl_professional_training` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_professional_training` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_program_studying`
--

DROP TABLE IF EXISTS `tbl_program_studying`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_program_studying` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_program_studying`
--

LOCK TABLES `tbl_program_studying` WRITE;
/*!40000 ALTER TABLE `tbl_program_studying` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_program_studying` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reference`
--

DROP TABLE IF EXISTS `tbl_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reference` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_business_address` text,
  `lb_home_address` text,
  `lb_phone_number` varchar(45) DEFAULT NULL,
  `lb_fax` varchar(45) DEFAULT NULL,
  `lb_email` varchar(105) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_reference_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reference`
--

LOCK TABLES `tbl_reference` WRITE;
/*!40000 ALTER TABLE `tbl_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_resume`
--

DROP TABLE IF EXISTS `tbl_resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_resume_title` varchar(255) DEFAULT NULL,
  `lb_resume_url` varchar(255) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1' COMMENT 'If value is 1 then the row is delete and 0 if no',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_resume`
--

LOCK TABLES `tbl_resume` WRITE;
/*!40000 ALTER TABLE `tbl_resume` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_skill`
--

DROP TABLE IF EXISTS `tbl_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_skill` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_skill_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_skill`
--

LOCK TABLES `tbl_skill` WRITE;
/*!40000 ALTER TABLE `tbl_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_working_history`
--

DROP TABLE IF EXISTS `tbl_working_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_working_history` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_working_history`
--

LOCK TABLES `tbl_working_history` WRITE;
/*!40000 ALTER TABLE `tbl_working_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_working_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_year`
--

DROP TABLE IF EXISTS `tbl_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_year` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_year`
--

LOCK TABLES `tbl_year` WRITE;
/*!40000 ALTER TABLE `tbl_year` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_year` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-13  8:45:40

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

// Public access to form registration
Route::get('/signup', 'PublicController@index');

Route::get('/register', function () {
    return view('home/register');
});

Route::resource('/candidates', 'CandidateController');
//Route::post('/account/create', 'PublicController@store');
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['web']], function() {
    Route::get('/candidates/register', 'CandidateRegisterController@index');
    Route::post('/candidates/register/create', 'CandidateRegisterController@register')->name('registerCandidate');
    Route::get('/candidate/last/apply', 'CandidateController@lastApplied')->name("applied");
    Route::get('/candidate/apply/job/{job_id}', 'CandidateController@applyto');
    Route::post('/candidate/apply/job/{job_id}', 'CandidateController@applyto');
    Route::get('/candidate/basic/', 'CandidateController@personalinfo');
    Route::post('/candidate/basic/', 'CandidateController@personalinfo');
    Route::get('/candidate/education', 'CandidateController@candidate_education');
    Route::post('/candidate/education', 'CandidateController@candidate_education');
    Route::get('/candidate/education/add', 'CandidateController@candidate_education_add');
    Route::post('/candidate/education/add', 'CandidateController@candidate_education_add');

    Route::get('/candidate/training', 'CandidateController@candidate_training');
    Route::post('/candidate/training', 'CandidateController@candidate_training');
    Route::get('/candidate/training/add', 'CandidateController@candidate_training_add');
    Route::post('/candidate/training/add', 'CandidateController@candidate_training_add');


    Route::get('/recruiters/register', 'RecruitRegisterController@index');
    Route::post('/recruiters/register/create', 'RecruitRegisterController@register')->name('registerRecruiter');
    
    Route::get('/employers/register', 'EmployerRegisterController@index');
    Route::post('/employers/register/create', 'EmployerRegisterController@register')->name('registerEmployer');

    Route::get('/category/list', 'JobCategoryController@index');
    Route::get('/post-job', 'PostJobController@index');
    Route::post('/post-job', 'PostJobController@store')->name('postjob');
    Route::get('/jobs-posted', 'PostJobController@passed');
    
    // Apply Jobs
    Route::get('/apply/jobs', 'FindJobController@index');
    
    Route::post('/skills/buildArray', 'PostJobController@buildSession');
    
    Route::get('/states/list', 'StatesController@getAll');
    
    Route::get('/cities/list', 'CitiesController@getAll');
    
    Route::post('/customer/skills/all', 'SkillsController@addSkillsByCustomers');
    
    Route::get('/list/jobs/search/', 'PostJobController@getAll');
    Route::post('/list/jobs/search/', 'PostJobController@getAll');

    //Route::get('/password/change', '');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@index');



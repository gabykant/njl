<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                </div>
                
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <th>Job title</th>
                            <th>Added on</th>
                            <th>City of work</th>
                            <th>Recruiter</th>
                        </thead>
                        @if(count($list_jobs) > 0)
                        <tbody>
                            @foreach($list_jobs as $list=>$v)
                                <tr>
                                    <td>{{ $v->lb_title }}</td>
                                    <td>{{ $v->lb_job_description }}</td>
                                    <td>{{ $v->lb_city_name }}</td>
                                    <td>{{ $v->lb_hiring_firstname }}</td>
                                </tr>
                            @endforeach
                        @else
                        <tr><td>No data to display</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
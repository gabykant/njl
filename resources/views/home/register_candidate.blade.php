@extends('layouts.welcome')
@section('current_title', ' - New Candidate')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">Register - New Candidate Profile</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('registerCandidate') }}">
                {{ csrf_field() }}

                <input id="profile" type="hidden" name="profile" value="candidate">

                <div class="form-group{{ $errors->has('lb_first_name') ? ' has-error' : '' }}">
                    <label for="lb_first_name" class="col-md-4 control-label">{{trans('form.lb_first_name')}}</label>

                    <div class="col-md-6">
                        <input id="lb_first_name" type="text" class="form-control" name="lb_first_name" value="{{ old('lb_first_name') }}" required autofocus>

                        @if ($errors->has('lb_first_name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_first_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_middle_name') ? ' has-error' : '' }}">
                    <label for="lb_middle_name" class="col-md-4 control-label">{{trans('form.lb_middle_name')}}</label>

                    <div class="col-md-6">
                        <input id="lb_middle_name" type="text" class="form-control" name="lb_middle_name" value="{{ old('lb_middle_name') }}" autofocus>

                        @if ($errors->has('lb_middle_name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_middle_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_last_name') ? ' has-error' : '' }}">
                    <label for="lb_last_name" class="col-md-4 control-label">{{trans('form.lb_last_name')}}</label>

                    <div class="col-md-6">
                        <input id="lb_last_name" type="text" class="form-control" name="lb_last_name" value="{{ old('lb_last_name') }}" required autofocus>

                        @if ($errors->has('lb_last_name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_last_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                        <a href="{{ url('/login') }}">Already have an account? Sign In</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
        </div></div></div>

@endsection
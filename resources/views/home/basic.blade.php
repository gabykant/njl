@extends('layouts.app')

@section('current_title', '')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Update user personal information</div>
        <div class="panel-body" >
            {!! Form::open( ['method'=>'post', 'role' => 'form', 'files' => true, 'class'=>'form-horizontal']) !!}
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('lb_gender_id') ? ' has-error' : '' }}">
                    <label for="lb_gender_id" class="col-md-4 control-label">{{trans('form.lb_gender')}}</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_gender_id', $gender, $userInformation[0]->lb_gender_id , ['class' => 'form-control', 'id' => 'lb_gender_id']) !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_first_name') ? ' has-error' : '' }}">
                    <label for="lb_first_name" class="col-md-4 control-label">{{trans('form.lb_first_name')}}</label>
                    <div class="col-md-6">
                        <input name="lb_first_name" type="text" value="{{ Auth::user()->lb_first_name }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_middle_name') ? ' has-error' : '' }}">
                    <label for="lb_middle_name" class="col-md-4 control-label">{{trans('form.lb_middle_name')}}</label>
                    <div class="col-md-6">
                        <input name="lb_middle_name" type="text" value="{{ Auth::user()->lb_middle_name }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_last_name') ? ' has-error' : '' }}">
                    <label for="lb_last_name" class="col-md-4 control-label">{{trans('form.lb_last_name')}}</label>
                    <div class="col-md-6">
                        <input name="lb_last_name" type="text"  value="{{ Auth::user()->lb_last_name }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_dob') ? ' has-error' : '' }}">
                    <label for="lb_dob" class="col-md-4 control-label">{{trans('form.lb_dob')}}</label>
                    <div class="col-md-6">
                        <input name="lb_dob" type="date"  value="{{ $userInformation[0]->lb_dob }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_primary_phone') ? ' has-error' : '' }}">
                    <label for="lb_primary_phone" class="col-md-4 control-label">{{trans('form.lb_primary_phone')}}</label>
                    <div class="col-md-6">
                        <input name="lb_primary_phone" type="number"  value="{{ $userInformation[0]->lb_primary_phone }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_fax') ? ' has-error' : '' }}">
                    <label for="lb_fax" class="col-md-4 control-label">{{trans('form.lb_fax')}}</label>
                    <div class="col-md-6">
                        <input name="lb_fax" type="text"  value="{{ $userInformation[0]->lb_fax }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_facebook_url') ? ' has-error' : '' }}">
                    <label for="lb_facebook_url" class="col-md-4 control-label">{{trans('form.lb_facebook_url')}}</label>
                    <div class="col-md-6">
                        <input name="lb_facebook_url" type="text"  value="{{ $userInformation[0]->lb_facebook_url }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_twitter_url') ? ' has-error' : '' }}">
                    <label for="lb_twitter_url" class="col-md-4 control-label">{{trans('form.lb_twitter_url')}}</label>
                    <div class="col-md-6">
                        <input name="lb_twitter_url" type="text"  value="{{ $userInformation[0]->lb_twitter_url }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_linkedin_url') ? ' has-error' : '' }}">
                    <label for="lb_linkedin_url" class="col-md-4 control-label">{{trans('form.lb_linkedin_url')}}</label>
                    <div class="col-md-6">
                        <input name="lb_linkedin_url" type="text"  value="{{ $userInformation[0]->lb_linkedin_url }}" class="form-control" />
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_extra_activity') ? ' has-error' : '' }}">
                    <label for="lb_extra_activity" class="col-md-4 control-label">{{trans('form.lb_extra_activity')}}</label>
                    <div class="col-md-6">
                        <textarea name="lb_extra_activity" class="form-control">{{ $userInformation[0]->lb_extra_activity }}</textarea>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_hobbies') ? ' has-error' : '' }}">
                    <label for="lb_hobbies" class="col-md-4 control-label">{{trans('form.lb_hobbies')}}</label>
                    <div class="col-md-6">
                        <textarea name="lb_hobbies" class="form-control">{{ $userInformation[0]->lb_hobbies }}</textarea>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_photo_url') ? ' has-error' : '' }}">
                    <label for="lb_photo_url" class="col-md-4 control-label">{{trans('form.lb_photo_url')}}</label>
                    <div class="col-md-6">
                        <input name="lb_photo_url" type="file"  class="form-control" />
                        @if (Storage::disk('public')->has($userInformation[0]->lb_photo_url))
                            <img src="{{ Storage::disk('registers/candidates')->get($userInformation[0]->lb_photo_url) }}" width="50px" height="50px" />
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('lb_photo_full_portrait') ? ' has-error' : '' }}">
                    <label for="lb_photo_full_portrait" class="col-md-4 control-label">{{trans('form.lb_photo_full_portrait')}}</label>
                    <div class="col-md-6">
                        <input name="lb_photo_full_portrait" type="file" class="form-control" />
                        @if (Storage::disk('public')->has($userInformation[0]->lb_photo_full_portrait))
                            <img src="{{ $userInformation[0]->lb_photo_full_portrait }}" width="50px" height="50px" />
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Update data
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
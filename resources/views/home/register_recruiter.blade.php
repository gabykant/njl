@extends('layouts.welcome')

@section('current_title', ' - New Recruiter')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            
    <div class="panel panel-default">
        <div class="panel-heading">Register new recruiter profile</div>
        <div class="panel-body">
            
            @if(session()->has('RegistrationError'))
                <p class='alert alert-danger'>
                    session()->get('RegistrationError');
                </p>
            @endif
            
            <form class="form-horizontal" role="form" method="POST" action="{{ route('registerRecruiter') }}">
                {{ csrf_field() }}

                <input id="profile" type="hidden" name="profile" value="recruiter">

                <div class="form-group{{ $errors->has('lb_hiring_firstname') ? ' has-error' : '' }}">
                    <label for="lb_hiring_firstname" class="col-md-4 control-label">{{trans('form.lb_hiring_firstname')}}</label>

                    <div class="col-md-6">
                        <input id="lb_hiring_firstname" type="text" class="form-control" name="lb_hiring_firstname" value="{{ old('lb_hiring_firstname') }}" required autofocus>

                        @if ($errors->has('lb_hiring_firstname'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_hiring_firstname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_hiring_middlename') ? ' has-error' : '' }}">
                    <label for="lb_hiring_middlename" class="col-md-4 control-label">{{trans('form.lb_hiring_middlename')}}</label>

                    <div class="col-md-6">
                        <input id="lb_hiring_middlename" type="text" class="form-control" name="lb_hiring_middlename" value="{{ old('lb_hiring_middlename') }}" autofocus>

                        @if ($errors->has('lb_hiring_middlename'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_hiring_middlename') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_hiring_lastname') ? ' has-error' : '' }}">
                    <label for="lb_hiring_lastname" class="col-md-4 control-label">{{trans('form.lb_hiring_lastname')}}</label>

                    <div class="col-md-6">
                        <input id="lb_hiring_lastname" type="text" class="form-control" name="lb_hiring_lastname" value="{{ old('lb_hiring_lastname') }}" required autofocus>

                        @if ($errors->has('lb_hiring_lastname'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_hiring_lastname') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                        <a href="{{ url('/login') }}">Already have an account? Sign In</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
        </div></div></div>

@endsection
@extends('layouts.app')

@section('current_title', '')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Add new education path 
            <div class="pull-right">
                <a href="{{ url('/candidate/education') }}" class=""><i class="glyphicon glyphicon-list"></i>List</a>
            </div>
        </div>
        <div class="panel-body" >
            <form method="post" role="form" class="form-horizontal">
                {{ csrf_field() }}
                
                <div class="form-group{{ $errors->has('lb_school_country') ? ' has-error' : '' }}">
                    <label for="lb_school_country" class="col-md-4 control-label">School Country</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_country', $countries, "", ['class' => 'form-control', 'id' => 'lb_country_id']) !!}

                        @if ($errors->has('lb_school_country'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_country') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_school_state') ? ' has-error' : '' }}">
                    <label for="lb_school_state" class="col-md-4 control-label">School State</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_state', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_state_id']) !!}

                        @if ($errors->has('lb_school_state'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_state') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_school_city') ? ' has-error' : '' }}">
                    <label for="lb_school_city" class="col-md-4 control-label">School City</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_city', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_city_id']) !!}

                        @if ($errors->has('lb_school_city'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_city') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_name') ? ' has-error' : '' }}">
                    <label for="lb_name" class="col-md-4 control-label">School Name</label>
                    <div class="col-md-6">
                        <input name="lb_name" type="text" value="" class="form-control" />
                        @if ($errors->has('lb_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_study_field_id') ? ' has-error' : '' }}">
                    <label for="lb_study_field_id" class="col-md-4 control-label">Major / Program name</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_study_field_id', $fields, null, ['class' => 'form-control', 'id' => 'lb_study_field_id']) !!}
                        @if ($errors->has('lb_study_field_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_study_field_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_degree_type_id') ? ' has-error' : '' }}">
                    <label for="lb_degree_type_id" class="col-md-4 control-label">Degree type</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_degree_type_id', $degree, null, ['class' => 'form-control', 'id' => 'lb_degree_type_id']) !!}
                    </div>
                </div>
                
                <!--<div class="form-group{{ $errors->has('lb_job_type_id') ? ' has-error' : '' }}">
                    <label for="lb_job_type_id" class="col-md-4 control-label">Type(fulltime/parttime)</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_job_type_id', $types, null, ['class' => 'form-control', 'id' => 'lb_job_type_id']) !!}
                    </div>
                </div>-->
                
                <div class="form-group{{ $errors->has('lb_graduated') ? ' has-error' : '' }}">
                    <label for="lb_graduated" class="col-md-4 control-label">Already graduated ?</label>
                    <div class="col-md-6">
                        <label><input type="radio" id="lb_graduated" name="lb_graduated" value='no' checked="checked"/>No</label>
                        <label><input type="radio" id="lb_graduated" name="lb_graduated" value='yes' />Yes</label>
                    </div>
                </div>
                
            <div id="graduated-yes">
                <div class="form-group{{ $errors->has('lb_attended_from') ? ' has-error' : '' }}">
                    <label for="lb_attended_from" class="col-md-4 control-label">Start date</label>
                    <div class="col-md-6">
                        <input name="lb_attended_from" type="date" value="<?php echo Date('Y-m-d');?>" class="form-control" />
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_graduation_date') ? ' has-error' : '' }}">
                    <label for="lb_graduation_date" class="col-md-4 control-label">Graduation date</label>
                    <div class="col-md-6">
                        <input name="lb_graduation_date" type="date" value="<?php echo Date('Y-m-d')?>" class="form-control" />
                    </div>
                </div>
            </div>
            <div id="graduated-no">
                <div class="form-group{{ $errors->has('lb_student_type_id') ? ' has-error' : '' }}">
                    <label for="lb_student_type_id" class="col-md-4 control-label">Student type</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_student_type_id', $studentType, null, ['class' => 'form-control', 'id' => 'lb_student_type_id']) !!}
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_grade_id') ? ' has-error' : '' }}">
                    <label for="lb_grade_id" class="col-md-4 control-label">Grade type</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_grade_id', $grades, null, ['class' => 'form-control', 'id' => 'lb_grade_id']) !!}
                        
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_attended_from_no') ? ' has-error' : '' }}">
                    <label for="lb_attended_from_no" class="col-md-4 control-label">Start date</label>
                    <div class="col-md-6">
                        <input name="lb_attended_from_no" type="date" value="<?php echo date('Y-m-d'); ?>" class="form-control" />
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_expected_graduation') ? ' has-error' : '' }}">
                    <label for="lb_expected_graduation" class="col-md-4 control-label">Expected graduation date</label>
                    <div class="col-md-6">
                        <input name="lb_expected_graduation" type="date" value="<?php echo date('Y-m-d'); ?>" class="form-control" />
                    </div>
                </div>
            </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Save data
                        </button>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>
@endsection
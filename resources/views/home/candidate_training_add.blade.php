@extends('layouts.app')

@section('current_title', '')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Add new training path 
            <div class="pull-right">
                <a href="{{ url('/candidate/training') }}" class=""><i class="glyphicon glyphicon-list"></i>List</a>
            </div>
        </div>
        <div class="panel-body" >
            <form method="post" role="form" class="form-horizontal">
                {{ csrf_field() }}
                
                <div class="form-group{{ $errors->has('lb_program_name') ? ' has-error' : '' }}">
                    <label for="lb_program_name" class="col-md-4 control-label">Program name</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_program_name', $certificates, "", ['class' => 'form-control', 'id' => 'lb_period_id']) !!}
                        @if ($errors->has('lb_program_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_program_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_program_length') ? ' has-error' : '' }}">
                    <label for="lb_program_length" class="col-md-4 control-label">Program length</label>
                    <div class="col-md-6">
                        <input name="lb_program_length" type="text" value="" class="form-control" />
                        {!! Form::select('lb_program_length_drop', ['day'=>'Day', 'month'=>'Month'], "", ['class' => 'form-control', 'id' => 'lb_period_id']) !!}
                        @if ($errors->has('lb_program_length'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_program_length') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_attended_from') ? ' has-error' : '' }}">
                    <label for="lb_attended_from" class="col-md-4 control-label">Start date</label>
                    <div class="col-md-6">
                        <input name="lb_attended_from" type="date" value="" class="form-control" />
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_attended_to') ? ' has-error' : '' }}">
                    <label for="lb_attended_to" class="col-md-4 control-label">End date</label>
                    <div class="col-md-6">
                        <input name="lb_attended_to" type="date" value="" class="form-control" />
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_graduated') ? ' has-error' : '' }}">
                    <label for="lb_certified" class="col-md-4 control-label">Certified ?</label>
                    <div class="col-md-6">
                        <label><input type="radio" name="lb_certified" value='' checked="checked"/>No</label>
                        <label><input type="radio" name="lb_certified" value='' />Yes</label>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_provider_name') ? ' has-error' : '' }}">
                    <label for="lb_provider_name" class="col-md-4 control-label">Training provider Name</label>
                    <div class="col-md-6">
                        <input name="lb_provider_name" type="text" value="" class="form-control" />
                        @if ($errors->has('lb_provider_name'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_provider_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_training_location') ? ' has-error' : '' }}">
                    <label for="lb_training_location" class="col-md-4 control-label">Training location</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_training_location', ['offline'=>'Offline', 'online'=>'Online'], "", ['class' => 'form-control', 'id' => 'location']) !!}

                        @if ($errors->has('lb_training_location'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_training_location') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

            <div id="location_offline">
                <div class="form-group{{ $errors->has('lb_training_website') ? ' has-error' : '' }}">
                    <label for="lb_training_website" class="col-md-4 control-label">Training website</label>
                    <div class="col-md-6">
                        <input name="lb_training_website" type="text" value="0" class="form-control" />
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_training_address') ? ' has-error' : '' }}">
                    <label for="lb_training_address" class="col-md-4 control-label">Training address</label>
                    <div class="col-md-6">
                        <input name="lb_training_address" type="text" value="0" class="form-control" />
                    </div>
                </div>
            </div>
            <div id="location_online">
                <div class="form-group{{ $errors->has('lb_school_country') ? ' has-error' : '' }}">
                    <label for="lb_school_country" class="col-md-4 control-label">Country</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_country', $countries, "", ['class' => 'form-control', 'id' => 'lb_country_id']) !!}

                        @if ($errors->has('lb_school_country'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_country') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_school_state') ? ' has-error' : '' }}">
                    <label for="lb_school_state" class="col-md-4 control-label">State</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_state', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_state_id']) !!}

                        @if ($errors->has('lb_school_state'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_state') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_school_city') ? ' has-error' : '' }}">
                    <label for="lb_school_city" class="col-md-4 control-label">City</label>
                    <div class="col-md-6">
                        {!! Form::select('lb_school_city', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_city_id']) !!}

                        @if ($errors->has('lb_school_city'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_school_city') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('lb_zip_code') ? ' has-error' : '' }}">
                    <label for="lb_zip_code" class="col-md-4 control-label">Zip code</label>
                    <div class="col-md-6">
                        <input name="lb_zip_code" type="text" value="0" class="form-control" />
                        
                    </div>
                </div>
                
                
                <div class="form-group{{ $errors->has('lb_training_address') ? ' has-error' : '' }}">
                    <label for="lb_training_address" class="col-md-4 control-label">Training address</label>
                    <div class="col-md-6">
                        <input name="lb_training_address" type="text" value="0" class="form-control" />
                    </div>
                </div>
            </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Save data
                        </button>
                    </div>
                </div>
            
            </form>
        </div>
    </div>
</div>
@endsection
@extends('layouts.header')


@section('title', 'Sign Up')

@section('content')

    {!! Form::model('') !!}
        <div class="logo"></div>
        <div class="login-block">
            <h1>User Login</h1>
            <input type="text" value="" placeholder="Username" id="username" />
            <input type="text" value="" placeholder="Full Name" id="name" />
            <input type="password" value="" placeholder="Password" id="password" />
            <input type="password" value="" placeholder="Confirm password" id="confirm_password" />
            Account Type<br />
            <select id="role_id" >
                <option value=1>Candidate</option>
                <option value=2>Recruiter</option>
                <option value=3>Employer</option>
            </select>
            <button>Submit</button>
            {{--{{ HTML::link('/admin', 'Already have an account? Sign In')}}--}}
        </div>
    {!! Form::close() !!}

@endsection
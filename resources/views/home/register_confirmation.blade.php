@extends('layouts.welcome')
@section('current_title', '')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">           
            <div class="panel panel-default">
                <div class="panel-heading">Confirmation registration</div>
                <div class="panel-body">
                    Your account has been registered successfully. <br />
                    We've sent an email with the validation link to your email address <strong>{{$email}}</strong>                    
                    <p>
                        <a href='{{url('/login')}}' class='btn btn-primary'>Log In</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.header')

@section('content')
    <div class="logo"></div>
    <div class="login-block">

        {!! Form::open(['url' => 'candidates', 'files' => true]) !!}

        {!! Form::label('lb_gender_id', 'Gender') !!}
        {!! Form::text('username') !!}

        {!! Form::label('lb_last_name', trans('form.lb_last_name')) !!}
        {!! Form::text('lb_last_name') !!}

        {!! Form::label('lb_first_name', trans('form.lb_first_name')) !!}
        {!! Form::text('lb_first_name') !!}

        {!! Form::label('lb_middle_name', trans('form.lb_first_name')) !!}
        {!! Form::text('lb_middle_name') !!}

        {!! Form::label('lb_dob', trans('form.lb_dob')) !!}
        {!! Form::date('lb_dob', \Carbon\Carbon::now()) !!}

        {!! Form::label('lb_email', trans('form.lb_email')) !!}
        {!! Form::email('lb_email') !!}

        {!! Form::label('lb_primary_phone', trans('form.lb_primary_phone')) !!}
        {!! Form::text('lb_primary_phone') !!}

        {!! Form::label('lb_fax', trans('form.lb_fax')) !!}
        {!! Form::text('lb_fax') !!}

        {!! Form::label('lb_facebook_url', trans('form.lb_facebook_url')) !!}
        {!! Form::text('lb_facebook_url') !!}

        {!! Form::label('lb_twitter_url', trans('form.lb_twitter_url')) !!}
        {!! Form::text('lb_twitter_url') !!}

        {!! Form::label('lb_linkedin_url', trans('form.lb_linkedin_url')) !!}
        {!! Form::text('lb_linkedin_url') !!}

        {!! Form::label('lb_extra_activity', trans('form.lb_extra_activity')) !!}
        {!! Form::text('lb_extra_activity') !!}

        {!! Form::label('lb_hobbies', trans('form.lb_hobbies')) !!}
        {!! Form::text('lb_hobbies') !!}

        {!! Form::label('lb_gender_id', trans('form.lb_first_name')) !!}
        {!! Form::text('lb_primary_phone') !!}

        {!! Form::label('lb_photo_url', trans('form.lb_photo_url')) !!}
        {!! Form::text('lb_photo_url') !!}

        {!! Form::label('lb_photo_full_portrait', trans('form.lb_photo_full_portrait')) !!}
        {!! Form::text('lb_photo_full_portrait') !!}

        {!! Form::label('lb_graduate_date', trans('form.lb_graduate_date')) !!}
        {!! Form::text('lb_graduate_date') !!}

        {!! Form::label('lb_program_studying_id', trans('form.lb_program_studying_id')) !!}
        {!! Form::text('lb_program_studying_id') !!}

        {!! Form::label('lb_degree_type_id', trans('form.lb_degree_type_id')) !!}
        {!! Form::text('lb_degree_type_id') !!}

        {!! Form::label('lb_start_date', trans('form.lb_start_date')) !!}
        {!! Form::text('lb_start_date') !!}

        {!! Form::label('lb_end_date', trans('form.lb_end_date')) !!}
        {!! Form::text('lb_end_date') !!}

        {!! Form::label('lb_job_type_id', trans('form.lb_job_type_id')) !!}
        {!! Form::text('lb_job_type_id') !!}

        {!! Form::label('lb_position', trans('form.lb_position')) !!}
        {!! Form::text('lb_position') !!}

        {!! Form::label('lb_department', trans('form.lb_department')) !!}
        {!! Form::text('lb_department') !!}

        {!! Form::label('lb_company_name', trans('form.lb_company_name')) !!}
        {!! Form::text('lb_company_name') !!}

        {!! Form::close() !!}

    </div>
@endsection

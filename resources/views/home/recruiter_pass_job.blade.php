@extends('layouts.app')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            Posted jobs
        </div>

        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Title</th><th>Type</th><th>Added on</th><th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($list_jobs) <= 0)
                        <tr><td colspan="4">No result was found</td></tr>
                    @endif
                    @foreach($list_jobs as $k=>$v)
                        <tr>
                            <td>{{$v->JTITLE}}</td>
                            <td>{{$v->TYNAME}}</td>
                            <td>{{$v->JCREATED}}</td>
                            <td><a href="{!! $v->JID !!}">View</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
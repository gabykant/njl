@extends('layouts.app')

@section('current_title', 'Post a Job')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Fill the following to post a new job</div>
        <div class="panel-body" >
            <form class="form-horizontal" role="form" method="POST" action="{{ route('postjob') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('lb_job_category_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_job_category_id', 'Job Category') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_job_category_id', $categories, null, ['class' => 'form-control', 'id' => 'lb_job_category_id']) !!}

                        @if ($errors->has('lb_job_category_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_job_category_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_title') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_title', 'Job title') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::input('text', 'lb_title', null, ['class' => 'form-control', 'id' => 'lb_title']) !!}

                        @if ($errors->has('lb_title'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_job_type_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_job_type_id', 'Job Type:') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_job_type_id', $types, null, ['class' => 'form-control']) !!}

                        @if ($errors->has('lb_job_type_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_job_type_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <!-- Adding Pay rate -->
                <div class="form-group{{ $errors->has('lb_payrate_amount') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_payrate_amount', 'Pay rate') !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::input('text', 'lb_payrate_amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::select('lb_curency', $curency, null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-1">Per</div>
                    <div class="col-md-3">
                        {!! Form::select('lb_period_id', $pay_rate_array, null, ['class' => 'form-control']) !!}

                        @if ($errors->has('lb_period_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_period_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <!-- Adding payment frequency -->
                <div class="form-group{{ $errors->has('lb_job_type_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_job_type_id', 'Pay frequency') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::select('lb_job_type_id', $pay_frequency, null, ['class' => 'form-control']) !!}

                        @if ($errors->has('lb_job_type_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_job_type_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_education_requirement') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_education_requirement', 'Education requirement') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_education_requirement', $degrees, null, ['class' => 'form-control']) !!}

                        @if ($errors->has('lb_education_requirement'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_education_requirement') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_pref_education_requirement') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_pref_education_requirement', 'Preferred education requirement') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_pref_education_requirement', $degrees, null, ['class' => 'form-control']) !!}

                        @if ($errors->has('lb_pref_education_requirement'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_pref_education_requirement') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_pref_education_requirement') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_education_requirement', 'Required Skills') !!}
                    </div>

                    <div class="col-md-6" id="forOnClick">
                        <table>
                            <div id="skills_list">

                            </div>
                        </table>
                        <a href="#" data-toggle="modal" data-target="#favoritesModal">Add required skills</a>
                    </div>
                </div>

                <!-- Adding job description -->
                <div class="form-group{{ $errors->has('lb_job_description') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_job_description', 'Job description') !!}
                    </div>

                    <div class="col-md-6">
                        {{ Form::textarea('lb_job_description', null, ['class' => 'form-control', 'rows' => 3, 'cols'=>70]) }}

                        @if ($errors->has('lb_job_description'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_job_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <!-- End Job Description -->

                <!-- Job Location -->
                <div class="form-group{{ $errors->has('lb_country_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_country_id', 'Country') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_country_id', $countries, "", ['class' => 'form-control', 'id' => 'lb_country_id']) !!}

                        @if ($errors->has('lb_country_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_country_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_state_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_state_id', 'State / Region / Province') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_state_id', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_state_id']) !!}

                        @if ($errors->has('lb_state_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_state_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('lb_city_id') ? ' has-error' : '' }}">
                    <div class="col-md-3">
                        {!! Form::Label('lb_city_id', 'City or Zip Code') !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::select('lb_city_id', array( 0 => "Choose from the list"), null, ['class' => 'form-control', 'id' => 'lb_city_id']) !!}

                        @if ($errors->has('lb_city_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lb_city_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Post Job
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<style>
    .modal-body{
        height: 200px;
    }
</style>

<div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        <h4 class="modal-title" id="favoritesModalLabel">Add a new skills</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" role="form">
              
              <div class="form-group{{ $errors->has('lb_skill_name') ? ' has-error' : '' }}">
                <div class="col-md-4">
                    {!! Form::Label('lb_skill_name', 'Choose from the list') !!} or <a href="#" data-toggle="modal" data-target="#modalAddSkills">add new skill</a>
                </div>
                <div class="col-md-8">
                    {!! Form::select('lb_skill_name', $skills, null, ['class' => 'form-control']) !!}
                    @if ($errors->has('lb_skill_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_skill_name') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
              <div class="form-group{{ $errors->has('lb_experience') ? ' has-error' : '' }}">
                <div class="col-md-4">
                    {!! Form::Label('lb_experience', 'Experience') !!}
                </div>
                <div class="col-md-4">
                    {!! Form::input('lb_experience', $degrees, null, ['class' => 'form-control', 'placeholder' => 'Example: 5', 'required' => 'required', 'id' => 'lb_experience']) !!}
                    @if ($errors->has('lb_experience'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_experience') }}</strong>
                        </span>
                    @endif
                </div>
                  <div class="col-md-4">
                    {!! Form::select('lb_skillsperiod_id', $skillsperiod, null, ['class' => 'form-control', 'id' => 'lb_skillsperiod_id']) !!}
                    @if ($errors->has('lb_skillsperiod_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lb_skillsperiod_id') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
          </form>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <span class="pull-right">
          <button type="button" class="btn btn-primary" id="jsAddSkill">
            Add to skills list
          </button>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalAddSkills" tabindex="-1" role="dialog" aria-labelledby="modalAddSkillsLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Add a new skill in the box</label>
                            <input type="text" class="form-control" id="new_skill" name="new_skill" />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_me">Close</button>
                <span class="pull-right">
                    <button type="button" class="btn btn-primary" id="jsAddSkillByUser">
                        Add a skill
                    </button>
                </span>
            </div>
            
        </div>
    </div>
</div>
@endsection
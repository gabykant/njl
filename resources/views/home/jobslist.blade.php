@extends('layouts.app')

@section('current_title', 'Find a Job')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Fill the following to post a new job</div>
        <div class="panel-body" >
            @foreach($allJobs as $jobs)
                <div id="" class="" style="border-bottom: 1px solid #000">
                    <h5>{{ $jobs->lb_title }}</h5>
                    <p>
                        {{ $jobs->lb_category_name }}
                        {{ $jobs->lb_country_name }}
                        {{ $jobs->lb_state_name }}
                        {{ $jobs->lb_degree_name }}
                        <span>{{ $jobs->lb_job_type_name }}</span>
                        <span>{{ str_limit($jobs->lb_job_description, 50) }}</span>
                    </p>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
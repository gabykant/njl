@extends('layouts.app')

@section('content')
<style>
.input-group-addon.input-sm {
  display: block;
}
.bloc_jobs{
    border: 1px solid #ccc;
    border-radius: 2px;
    }
</style>
<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            Applied jobs
        </div>

        <div class="panel-body">
            @if(count($list_jobs) <= 0)
                No result was found
            @endif
            @foreach($list_jobs as $k=>$v)
                <div class="bloc_jobs" style="border: 1px solid #CCC; border-radius: 5px; margin: 20px; padding: 1%">
                    <p style="border-bottom: 1px solid #ccc">
                        Candidate: {{$v->lb_candidate_id}} <br/>
                        Job position id : {{$v->lb_job_id}}
                    </p>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

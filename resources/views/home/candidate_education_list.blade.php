@extends('layouts.app')

@section('current_title', '')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">Your education 
            <div class="pull-right">
                <a href="{{ url('/candidate/education/add') }}" class=""><i class="glyphicon glyphicon-plus"></i>Add</a></div></div>
        <div class="panel-body" >
            <table class="table table-response">
                <thead>
                    <tr>
                        <th>School</th>
                        <th>Education</th>
                        <th>Start date</th>
                        <th>End date</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if(count($list_edu) <= 0 )
                        <tr><td colspan="5">No data was found</td></tr>
                    @else
                        @foreach($list_edu as $list)
                            <tr>
                                <td>{{$list->lb_name}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                
            </table>
        </div>
    </div>
</div>

@endsection
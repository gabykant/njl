@extends('layouts.app')

@section('content')

<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            Apply to this job position ?
        </div>

        <div class="panel-body">
            
            @if (session('save_apply_job'))
                <div class="alert alert-success">
                    {{ session('save_apply_job') }}
                </div>
            @endif
            
            <form method="post">
                {{ csrf_field() }}
                <p>
                    <span>Title:</span><span>{{$title}}</span>
                </p>
                <p>
                    <span>Description:</span><span>{{$description}}</span>
                </p
                <p>
                    <span>Type:</span><span>{{$type}}</span>
                </p>
                <p>
                    <span>Education:</span><span>{{$education}}</span>
                </p>
                <p>
                    <span>Place of work:</span><span>{{$city}} / {{$state}} / {{$country}}</span>
                </p>
                <p>
                    <span>Recruiter:</span><span>{{$recruiter}}</span>
                </p>
                <p>
                    <span>Added on:</span><span>{{$created_on}}</span>
                </p>
                <input type="hidden" name="lb_job_id" value="{{$id}}" />
                
                @if($checked > 0)
                <span style="background-color: red; color: #FFF">You have already applied to this job position</span>. <a href="{{url('/home')}}">Go back</a>
                @else
                <button class="btn btn-primary">Apply Now</button>
                @endif
            </form>
            
        </div>
    </div>
</div>
@endsection
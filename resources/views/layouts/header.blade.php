<!DOCTYPE html>
<html>
	<head>
		<title>Welcome to the platform - Registration</title>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href="{{ asset('/css/njltheme.css') }}" rel="stylesheet">
		<meta charset="UTF-8">
		<script type="text/javascript" src="../libs/js/vue.js"></script>
	</head>
<body>

	<div class="container">
		@yield('content')
	</div>

</body>
</html>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.header_title', 'National Jobs List') }}</title>

    <!-- Styles -->
    <link href="{{ asset('packages/admin/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">National Jobs List
                        @yield('current_title')
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
                            {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->lb_first_name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container" xmlns:v-on="http://www.w3.org/1999/xhtml">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            My actions
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                @if(Auth::user()->lb_role_id != 1)
                                    <li><a href="{{ url('post-job') }}">Post a new job</a></li>
                                    <li><a href="{{ url('jobs-posted') }}">Pass posted jobs</a></li>
                                    <li><a href="{{ route('login') }}">Connect to Employers</a></li>
                                @else
                                    <li><a href="{{ route('home') }}">Find a job</a></li>
                                    <li><a href="{{ route('applied') }}">Last applied job</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    
                    @if(Auth::user()->lb_role_id == 1)
                    <div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Profile
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                <li role="presentation"><a href="{{ url('/candidate/basic/') }}"><i class="fa fa-lock"></i>&nbsp;&nbsp;Personal information</a></li>
                                <li role="presentation">
                                    <a href="{{ url('/candidate/education') }}"><i class="fa fa-lock"></i>&nbsp;&nbsp;Education</a></li>
                                <li role="presentation"><a href="{{ url('/candidate/training') }}"><i class="fa fa-lock"></i>&nbsp;&nbsp;Training</a></li>
                            </ul>
                        </div>
                    </div>
                    @endif
                    
                    <!--<div class="panel panel-default panel-flush">
                        <div class="panel-heading">
                            Settings
                        </div>
                        <div class="panel-body">
                            <ul class="nav">
                                <li role="presentation"><a href="{{ url('/password/reset') }}"><i class="fa fa-lock"></i>&nbsp;&nbsp;Security</a></li>
                            </ul>
                        </div>
                    </div>-->
                    
                    
                </div>
                
                @yield('content')
                
            </div>
        </div>
        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!--    Scripts for the common functions -->
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>

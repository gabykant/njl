@extends('layouts.app')

@section('content')
<style>
.input-group-addon.input-sm {
  display: block;
}
.bloc_jobs{
    border: 1px solid #ccc;
    border-radius: 2px;
    }
</style>
<div class="col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <form method="POST">
                <div class="form-group">
                  <div class="input-group">
                      <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
                        <input name="search_list" class="form-control input-sm" type="text" placeholder="Search" value="{{ old('search_list') }}" />
                        <div class="input-group-addon input-sm">
                            <button class="glyphicon glyphicon-search"></button>
                        </div>
                  </div>
                </div>
            </form>
        </div>

        <div class="panel-body">
            @if(count($list_jobs) <= 0)
                No result was found
            @endif
            @foreach($list_jobs as $k=>$v)
            <div class="bloc_jobs" style="border: 1px solid #CCC; border-radius: 5px; margin: 20px; padding: 1%">
                <p style="border-bottom: 1px solid #ccc">
                    {{$v->JTITLE}}
                </p>
                <ul style="list-style: none">
                    <li>Description: {{$v->JDESC}}</li>
                    <li>Position: {{$v->TYTYPENAME}}</li>
                    <li>Education requirement: {{$v->EEDUNAME}}</li>
                    <li>City/State/Country of position:{{$v->CCITYNAME}}, {{$v->SSTATENAME}} / {{$v->CCCOUNTRYNAME}}</li>
                    <li>Posted by: {{$v->RERECRUITERNAME}} - {{$v->CREATEDAT}}</li>
                    
                    <li><a href="{{ url('/candidate/apply/job', ['job_id' => $v->JID ]) }}" style="background: #ccc; text-color: #FFF">Apply Now</a></li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

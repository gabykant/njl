$(document).ready(function(){

    $('#add_education').click(function(e){
        e.preventDefault();
        var inp = $('#box');
        var i = $('input').length + 1;

        $('<div id="box' + i +'">' +
              '<div class="row">' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +
                        {!! Form::Label('lb_education_requirement', 'Skills', array('class' => 'Form_section_heading')) !!}
                    '</div>' +
                    '<div class="col-md-9">' +
                        '<input type="text" name="school_name[]" id="" class="form-control Form_element" placeholder="School">' +
                    '</div>' +

                '</div>' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +

                    '</div>' +
                    '<div class="col-md-8">' +
                        '<input type="text" name="program[]" id="" class="form-control Form_element" placeholder="Program">' +
                    '</div>' +
                '</div>' +
                '<div class="col-md-12">' +
                    '<div class="col-md-3">' +

                    '</div>' +
                    '<div class="col-md-4">' +
                        '<input type="text" name="start_date[]" id="" class="form-control Form_element" placeholder="Start">' +
                    '</div>' +
                    '<div class="col-md-4">' +
                        '<input type="text" name="end_date[]" id="" class="form-control Form_element" placeholder="End">' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>').appendTo(inp);
        i++;
    });

    $('body').on('click','#remove',function(){
        $(this).parent('div').remove();
    });
    
    $('#jsAddSkill').on('click', function(){
        alert();
    });

});
<?php
/**
 * Created by PhpStorm.
 * User: itok
 * Date: 6/22/2017
 * Time: 12:30 AM
 */

return [
    'lb_last_name' => 'Last name',
    'lb_first_name' => 'First name',
    'lb_middle_name' => 'Middle name',
    'lb_hiring_firstname'   => 'HR first name',
    'lb_hiring_lastname'   => 'HR last name',
    'lb_hiring_middlename'   => 'HR middle name',
    'lb_employee_firstname'   => 'Employer first name',
    'lb_employee_middlename'   => 'Employer last name',
    'lb_employee_lastname'   => 'Employer middle name',
    'lb_description'    => 'Category description',
    'lb_category_name'  =>  'Category name',
    'jobcategory'   => 'Job Category',
    'lb_status' => 'Etat',
    'edit' => 'Edit',
    'lb_degree_name'    =>  'Degree name',
    'lb_degree'    =>  'Degree Type',
    'lb_slot_name' => 'Salary Payment',
    'lb_skills_name'    =>  'Skills Name',
    'lb_pay_frequency_name' => 'Pay Frequency',
    'lb_pay_frequency'  => 'Pay frequency',
    'lb_state_code' => 'State Code',
    'lb_state_name' =>  'State name',
    'lb_country_code'   =>  'Country Code',
    'lb_country_name'   =>  'Country Name',
    'upload_file'   =>  'Upload file',
    'lb_job_type_name'  => 'Job Type',
    'lb_grade_name' => 'Grade name',
    'lb_school_name' => 'School name',
    'lb_country' => 'Country',
    'lb_state' => 'State',
    'lb_city' => 'City',
    'job_location' => 'Job Location Name',
    'certificate_name' => 'Certification name',
    'lb_major_name' => 'Major/Program name',
    'lb_gender' => 'Gender',
    'lb_dob' => 'Date of birth',
    'lb_primary_phone' => 'Primary phone contact',
    'lb_fax' => 'Fax',
    'lb_facebook_url' => 'Facebook link',
    'lb_twitter_url' => 'Twitter link',
    'lb_linkedin_url' => 'LinkedIn Url',
    'lb_extra_activity' => 'Extra Activities',
    'lb_hobbies' => 'Hobbies',
    'lb_photo_url' => 'Picture',
    'lb_photo_full_portrait' => 'Photo portrait'
];
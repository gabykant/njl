<?php

namespace App\Admin\Extensions;

use Encore\Admin\Admin;

class ActivateCountry{
    
    protected $id;
    
    protected $status;

    public function __construct($id, $status)
    {
        $this->id = $id;
        $this->status = $status;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-check-row').on('click', function () {

    // Your code.
    // console.log($(this).data('id') + "Status" + $(this).data('status'));
    
    $.ajax({
        method: 'post',
        url: '/admin/country/activatedesactivate',
        data: {
            _token:LA.token,
            ids: selectedRows(),
            id:$(this).data('id'),
            status: $(this).data('status')
        },
        success: function () {
            $.pjax.reload('#pjax-container');
            toastr.success('Done');
        }
    });

});

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        
        $render = "<a class='btn btn-xs btn-success grid-check-row' data-id='{$this->id}' data-status=1>Enable</a>";
        
        if($this->status == 1){
            $render = "<a class='btn btn-xs btn-warning grid-check-row' data-id='{$this->id}' data-status=0>Disable</a>";
        }

        return $render;
    }

    public function __toString()
    {
        return $this->render();
    }
}
<?php

use Illuminate\Routing\Router;

Admin::registerHelpersRoutes();

Route::group([
    'prefix'        => config('admin.prefix'),
    'namespace'     => Admin::controllerNamespace(),
    'middleware'    => ['web', 'admin'],
],function(Router $router) {

    $router->get('/', 'HomeController@index');

    // Route for candidates
    /*$router->get('/candidates', 'CandidateController@index');
    $router->get('/candidates/create', 'CandidateController@create');
    $router->post('/candidates', 'CandidateController@store');

    // Route for recruiter
    $router->get('/recruiters', 'RecruiterController@index');
    $router->get('/recruiters/create', 'RecruiterController@create');
    $router->post('/recruiters', 'RecruiterController@store');*/

    // get list of Gender
    $router->get('/api/genders', 'GenderController@genders');

    $router->get('/categories', 'JobCategoryController@index');
    $router->get('/categories/create', 'JobCategoryController@create');
    $router->post('/categories', 'JobCategoryController@store');
    $router->get('/categories/{category}/edit', 'JobCategoryController@edit');
    $router->put('/categories/{category}', 'JobCategoryController@update');
    $router->delete('/categories/{category}', 'JobCategoryController@destroy');

    $router->get('/degrees', 'DegreeTypeController@index');
    $router->get('/degrees/create', 'DegreeTypeController@create');
    $router->post('/degrees', 'DegreeTypeController@store');
    $router->get('/degrees/{category}/edit', 'DegreeTypeController@edit');
    $router->put('/degrees/{category}', 'DegreeTypeController@update');
    $router->delete('/degrees/{category}', 'DegreeTypeController@destroy');

    $router->get('/pay-rate', 'SalaryPaymentController@index');
    $router->get('/pay-rate/create', 'SalaryPaymentController@create');
    $router->post('/pay-rate', 'SalaryPaymentController@store');
    $router->get('/pay-rate/{category}/edit', 'SalaryPaymentController@edit');
    $router->put('/pay-rate/{category}', 'SalaryPaymentController@update');
    $router->delete('/pay-rate/{category}', 'SalaryPaymentController@destroy');

    $router->get('/skills-level', 'SkillsPeriodController@index');
    $router->get('/skills-level/create', 'SkillsPeriodController@create');
    $router->post('/skills-level', 'SkillsPeriodController@store');
    $router->get('/skills-level/{category}/edit', 'SkillsPeriodController@edit');
    $router->put('/skills-level/{category}', 'SkillsPeriodController@update');
    $router->delete('/skills-level/{category}', 'SkillsPeriodController@destroy');

    Route::resource('/pay-frequency', 'PayFrequencyController');
    Route::resource('/countries', 'CountryController');
    $router->get('/api/country/list', 'CountryController@getCountries');
    $router->get('/api/state/list', 'StateController@getStatesByCountry');
    $router->get('/api/city/list', 'CityController@getCitiesByStates');
    $router->get('/api/states', 'StateController@getStates');
    $router->post('/country/activatedesactivate', 'CountryController@ActivateDesactivate');
    
    Route::resource('/states', 'StateController');
    Route::resource('/cities', 'CityController');

    Route::resource('/jobtypes', 'JobTypeController');
    
    Route::resource('/currency', 'CurrencyController');
    
    Route::resource('/skills', 'SkillsController');
    
    Route::resource('/educations', 'EducationController');
    
    Route::resource('/jobs', 'JobsController');
    
    Route::resource('/candidates', 'CandidateController');

    Route::resource('/student-level', 'GradesController');
    
    Route::resource('/schools', 'SchoolController');
    Route::get('/schools/list', 'SchoolController@schools');

    Route::resource('/locations', 'JobLocationController');
    Route::resource('certificate-types', 'CertificateTypeController');

    Route::resource('/recruiters', 'RecruiterController');
    Route::resource('/employers', 'EmployeeController');

    Route::resource('/major-program', 'MajorProgramController');
});

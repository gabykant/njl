<?php

namespace App\Admin\Controllers;

use App\Models\TblSchool;
use App\Models\TblCountry;
use App\Models\TblStates;
use App\Models\TblCity;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\DB;

class SchoolController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TblSchool::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            
            $grid->lb_school_name('School name');
            //$grid->lb_country_

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TblSchool::class, function (Form $form) {

            $form->display('id', 'ID');
            
            $form->text('lb_school_name', trans('form.lb_school_name'))->rules('required');
            $form->select('lb_country_id', trans('form.lb_country'))
                    ->options(function($id) {
                        $countries = TblCountry::find($id);

                        if ($countries) {
                            return [$countries->id => $countries->lb_country_name];
                        }
                    })->ajax('/admin/api/country/list')->load('lb_state_id', '/admin/api/state/list')->rules('required');
            
            $form->select('lb_state_id', trans('form.lb_state'))
                    ->options(function($id){
                        $states = TblStates::find($id);
                        if ($states) {
                            return [$states->id => $states->text];
                        }
                    })->load('lb_city_id', '/admin/api/city/list')->rules('required');

            $form->select('lb_city_id', trans('form.lb_city'))
                ->options(function($id){
                    $cities = TblCity::find($id);
                    if($cities){
                        return [$cities->id => $cities->text];
                    }
                })->rules('required');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
    
    public function schools(Request $request)
    {
        $q = $request->get('q');
        return TblSchool::where('lb_school_name', 'like', "%$q%")->paginate(null, ['id', 'lb_school_name as text']);
    }
}
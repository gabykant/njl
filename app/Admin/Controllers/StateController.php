<?php

namespace App\Admin\Controllers;

use App\Models\TblCountry;
use App\Models\TblStates;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;

class StateController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('header');
            $content->description('description');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('header');
            $content->description('description');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TblStates::class, function (Grid $grid) {
            //$grid->lb_state_code(trans('form.lb_state_code'))->sortable();
            $grid->lb_state_name(trans('form.lb_state_name'))->sortable();
            $grid->lb_country_code(trans('form.lb_country_code'))->sortable();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TblStates::class, function (Form $form) {
            $form->display('id', 'ID');

            $form->select('lb_country_code', trans('form.lb_country_name'))->options(function ($id) {
                $user = TblCountry::find($id);

                if ($user) {
                    return [$user->lb_country_code => $user->lb_country_name];
                }
            })->ajax('/admin/api/country/list');

            $form->textarea('lb_state_name', trans('form.upload_file'))->rows(10)->rules('required');
            
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $name = $request->input('lb_state_name');
        echo $name;
    }
    public function getStates(\Illuminate\Http\Request $request){
        $q = $request->get('q');
        return TblStates::where('lb_state_name', '=', "%$q%")->paginate(null, ['id', 'lb_state_name as text']);        
    }
    public function getStatesByCountry(\Illuminate\Http\Request $request){
        $q = $request->get('q');
        return TblStates::where('lb_country_code', "$q")->get(['id', DB::raw('lb_state_name as text')]);        
    }
    
    /*public function getAll(Request $request){
        $lb_state_id = $request->get('lb_country_id');
        $all_states = TblStates::where('lb_country_code', '=', $lb_state_id)->get();
        return $all_states;
    }*/
}

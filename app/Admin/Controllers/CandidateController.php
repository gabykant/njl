<?php

namespace App\Admin\Controllers;

use App\Models\Gender;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\DB;

use App\Models\Candidate;

use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Chart\Bar;
use Encore\Admin\Widgets\Chart\Doughnut;
use Encore\Admin\Widgets\Chart\Line;
use Encore\Admin\Widgets\Chart\Pie;
use Encore\Admin\Widgets\Chart\PolarArea;
use Encore\Admin\Widgets\Chart\Radar;
use Encore\Admin\Widgets\Collapse;
use Encore\Admin\Widgets\InfoBox;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\MessageBag;

class CandidateController extends Controller
{
    use ModelForm;

    public $candidates = array();
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = $this->get_candidates();
        for($i=0; $i<count($candidates); $i++){
            array_push($this->candidates, array(
                $candidates[$i]->lb_first_name,
                $candidates[$i]->lb_last_name,
                $candidates[$i]->email,
                $candidates[$i]->created_at
            ));
        }
        return Admin::content(function (Content $content) {
            $content->header(trans('admin::lang.candidate'));
            $content->description(trans('admin::lang.list'));
            $content->body('');
            $content->row((new Box('List of all candidates', 
            new Table(['First Name', 'Last Name', 'Email Address', 'Created at'], $this->candidates)))->style('info')->solid());
        
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('admin::lang.candidate'));
            $content->description(trans('admin::lang.create'));
            $content->body($this->form());
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Update candidate profile');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function grid()
    {
        return Candidate::grid(function (Grid $grid) {
            $grid->id('ID')->sortable();
            //$grid->lb_user_id(trans('admin::lang.lb_user_id'));
            //$grid->lb_gender_id(trans('admin::lang.lb_gender_id'));
            $grid->lb_last_name(trans('admin::lang.lb_last_name'));
            $grid->lb_first_name(trans('admin::lang.lb_first_name'));
            //$grid->lb_middle_name(trans('admin::lang.lb_middle_name'));
            //$grid->lb_dob(trans('admin::lang.lb_dob'));
            $grid->lb_email(trans('admin::lang.lb_email'));
            $grid->lb_primary_phone(trans('admin::lang.lb_primary_phone'));
            /*$grid->lb_fax(trans('admin::lang.lb_fax'));
            $grid->lb_facebook_url(trans('admin::lang.lb_facebook_url'));
            $grid->lb_twitter_url(trans('admin::lang.lb_twitter_url'));
            $grid->lb_linkedin_url(trans('admin::lang.lb_linkedin_url'));
            $grid->lb_extra_activity(trans('admin::lang.'));
            $grid->lb_hobbies(trans('admin::lang.lb_hobbies'));
            $grid->lb_photo_url(trans('admin::lang.lb_photo_url'));
            $grid->lb_graduate_date(trans('admin::lang.lb_graduate_date'));
            $grid->lb_program_studying_id(trans('admin::lang.lb_program_studying_id'));
            $grid->lb_degree_type_id(trans('admin::lang.lb_degree_type_id'));
            $grid->lb_start_date(trans('admin::lang.lb_start_date'));
            $grid->lb_end_date(trans('admin::lang.lb_end_date'));
            $grid->lb_job_type_id(trans('admin::lang.lb_job_type_id'));
            $grid->lb_position(trans('admin::lang.lb_position'));
            $grid->lb_department(trans('admin::lang.lb_department'));
            $grid->lb_company_name(trans('admin::lang.lb_company_name'));*/

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        return Admin::form(Candidate::class, function (Form $form) {
            $form->display('id', 'ID');

            $form->select('lb_gender_id', trans('admin::lang.lb_gender_id'))->options(function ($id) {
                $user = Gender::find($id);

                if ($user) {
                    return [$user->id => $user->lb_name_gender];
                }
            })->ajax('/admin/api/genders')->rules('required');

            //$form->('lb_user_id', trans('admin::lang.'));
            //$form->('lb_gender_id', trans('admin::lang.'));
            $form->text('lb_last_name', trans('admin::lang.lb_last_name'))->rules('required');
            $form->text('lb_first_name', trans('admin::lang.lb_first_name'))->rules('required');
            $form->text('lb_middle_name', trans('admin::lang.lb_middle_name'));
            $form->date('lb_dob', trans('admin::lang.lb_dob'))->rules('required');
            $form->text('lb_email', trans('admin::lang.lb_email'))->rules('required');
            $form->mobile('lb_primary_phone', trans('admin::lang.lb_primary_phone'));
            $form->text('lb_fax', trans('admin::lang.lb_fax'));
            $form->text('lb_facebook_url', trans('admin::lang.lb_facebook_url'));
            $form->text('lb_twitter_url', trans('admin::lang.lb_twitter_url'));
            $form->text('lb_linkedin_url', trans('admin::lang.lb_linkedin_url'));
            $form->text('lb_extra_activity', trans('admin::lang.lb_extra_activity'));
            $form->text('lb_hobbies', trans('admin::lang.lb_hobbies'));
            $form->image('lb_photo_url', trans('admin::lang.lb_photo_url'));
            $form->datetime('lb_graduate_date', trans('admin::lang.lb_graduate_date'))->format('YYYY-MM-DD HH:mm:ss');
            //$form->('lb_program_studying_id', trans('admin::lang.'));
            //$form->('lb_degree_type_id', trans('admin::lang.'));
            $form->dateRange('lb_start_date', 'lb_end_date', trans('admin::lang.lb_start_date'))->format('YYYY-MM-DD HH:mm:ss');
            /*$form->('lb_job_type_id', trans('admin::lang.'));
            $form->('lb_position', trans('admin::lang.'));
            $form->('lb_department', trans('admin::lang.'));*/
            $form->text('lb_company_name', trans('admin::lang.lb_company_name'));

            /*   $form->datetime('issued_on', trans('admin::lang.magazine_issue_on'))->rules('required')->format('YYYY-MM-DD HH:mm:ss');
               $form->text('file_name', trans('admin::lang.magazine_file_name'))->rules('required');
               $form->text('file_path', trans('admin::lang.magazine_file_path'))->rules('required');
               $form->datetime('added_on', trans('admin::lang.magazine_added_on'))->rules('required')->format('YYYY-MM-DD HH:mm:ss');
               $form->decimal('price', trans('admin::lang.magazine_price'))->rules('required');
               $form->image('images', trans('admin::lang.magazine_images'))->rules('required');
               $form->textarea('overview', trans('admin::lang.magazine_overview'))->rules('required');
               $form->image('cover_image_name', trans('admin::lang.magazine_cover_image_name'))->rules('required');
               $form->image('cover_thumb_image_name', trans('admin::lang.magazine_cover_thumb_image_name'))->rules('required');
   */
            $form->saved(function (Form $form) {

                $error = new MessageBag([
                    'title'   => trans('admin::lang.title_save_failed'),
                    'message' => trans('admin:lang.save_failed'),
                ]);

                return back()->with(compact('error'));
            });

            $form->saved(function(Form $form) {
                $success = new MessageBag([
                    'title'   => trans('admin::lang.title_save_succeeded'),
                    'message' => trans('admin::lang.save_succeeded'),
                ]);

                return back()->with(compact('success'));
            });
        });
    }

    private function get_candidates(){
        return DB::table('Users as U')->where(['U.lb_role_id'=>1])/*->join('tbl_candidate as C', 'U.id', '=', 'C.lb_user_id')*/->get();
    }
}

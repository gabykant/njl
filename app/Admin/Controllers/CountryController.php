<?php

namespace App\Admin\Controllers;

use App\Models\TblCountry;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Tools;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

use App\Admin\Extensions\ActivateCountry;

class CountryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('header');
            $content->description('description');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form_edit_active()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TblCountry::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->lb_country_name(trans('form.lb_country_name'))->sortable();
            $grid->lb_country_code(trans('form.lb_country_code'))->sortable();
            
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
                $actions->append(new ActivateCountry($actions->getKey(), $actions->row->lb_active));
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TblCountry::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('lb_country_name', trans('form.lb_country_name'))->rules('required');
            $form->text('lb_country_code' , trans('form.lb_country_code'))->rules('required');
            $form->hidden('lb_active')->value(0);

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    protected function form_edit_active()
    {
        return Admin::form(TblCountry::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('lb_country_name', trans('form.lb_country_name'))->rules('required');
            $form->text('lb_country_code' , trans('form.lb_country_code'))->rules('required');
            $form->select('lb_active', trans('form.lb_active'))->options([0 => 'Inactive', 1 => 'Active']);

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function getCountries(Request $request)
    {
        $q = $request->get('q');

        return TblCountry::where('lb_country_name', 'like', "%$q%")->paginate(null, ['id', 'lb_country_name as text']);
    }
    
    public function ActivateDesactivate(Request $request){
        $id = $request->get('id');
        $status = $request->get('status');
        return TblCountry::where('id', '=', $id)->update(['lb_active' => $status]);
    }
}

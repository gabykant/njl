<?php

namespace App\Admin\Controllers;

use App\Models\TblPostJob;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;


use Illuminate\Support\MessageBag;

class JobsController extends Controller
{
    use ModelForm;
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /*public function index(){
        $data['list_jobs'] = DB::table("tbl_job")->orderby("id", "DESC")->get();
        //var_dump($data);
        return view('admin.list_jobs', $data);
    }*/
    public function index(){
        /*$list_jobs = DB::table("tbl_job AS J")->orderby("J.id", "DESC")->join('tbl_recruiter AS R', "R.id", "=", "J.lb_recruiter_id")->get();
        var_dump($list_jobs);*/
        return Admin::content(function (Content $content) {
            $content->header(trans('admin::lang.candidate'));
            $content->description(trans('admin::lang.list'));
            $data['list_jobs'] = DB::table("tbl_job AS J")
                ->orderby("J.id", "DESC")
                ->join('tbl_cities AS C', "C.id", "=", "J.lb_city_id")
                ->join('tbl_recruiter AS R', "R.id", "=", "J.lb_recruiter_id")
                //->select("C.id AS CID, J.lb_title AS JTITLE, J.lb_job_description AS JDEC")
                ->get();
            //var_dump($data);
            $view = view('admin.list_jobs', $data);
            $content->body($view);
        });
    }
    
    protected function grid()
    {
        return TblPostJob::grid(function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->lb_title(trans('admin::lang.lb_last_name'));
            $grid->lb_first_name(trans('admin::lang.lb_first_name'));
            $grid->lb_email(trans('admin::lang.lb_email'));
            $grid->lb_primary_phone(trans('admin::lang.lb_primary_phone'));

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->disableExport();
        });
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\JobCategory;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

use Illuminate\Support\MessageBag;

class JobCategoryController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('form.jobcategory'));
            $content->description(trans('admin::lang.list'));
            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header(trans('form.jobcategory'));
            $content->description(trans('admin::lang.list'));
            $content->body($this->form());
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header(trans('form.jobcategory'));
            $content->description(trans('form.edit'));
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function grid()
    {
        return Admin::grid(JobCategory::class, function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->lb_category_name(trans('form.lb_category_name'));
            $grid->lb_is_delete(trans('form.lb_status'));

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                if ($actions->getKey() == 1) {
                    $actions->disableDelete();
                }
            });

            $grid->tools(function (Grid\Tools $tools) {
                $tools->batch(function (Grid\Tools\BatchActions $actions) {
                    $actions->disableDelete();
                });
            });

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(JobCategory::class, function (Form $form) {

            $form->display('id', 'ID');
            
            $form->text('lb_category_name', trans('form.lb_category_name'))->rules('required');
            $form->textarea('lb_description', trans('form.lb_description'));

//            $form->display('created_at', 'Created At');
//            $form->display('updated_at', 'Updated At');
        });
    }

    public function getCategories(Request $request)
    {
        $q = $request->get('q');

        return JobCategory::where('lb_category_name', 'like', "%$q%")->paginate(null, ['id', 'lb_category_name as text']);
    }
}

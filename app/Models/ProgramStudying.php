<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramStudying extends Model
{
    protected $table = 'tbl_program_studying';
}

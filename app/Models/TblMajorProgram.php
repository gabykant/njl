<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblMajorProgram extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_major_program';
}

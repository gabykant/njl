<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblUserRole extends Model
{
    protected $table = 'tbl_role';
}

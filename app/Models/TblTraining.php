<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblTraining extends Model
{
    protected $fillable = [
        'lb_candidate_id',
        'lb_program_name',
        'lb_program_length',
        'lb_program_length_drop',
        'lb_attended_from',
        'lb_attended_to',
        'lb_graduated',
        'lb_provider_name',
        'lb_training_location',
        'lb_training_website',
        'lb_training_address',
        'lb_school_country',
        'lb_school_state',
        'lb_school_city',
        'lb_zip_code'
    ];
    
    protected $table = "tbl_professional_training";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}

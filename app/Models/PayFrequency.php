<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayFrequency extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_pay_frequency';
}

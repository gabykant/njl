<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostJobSkill extends Model
{
    protected $table = "tbl_postjob_skills";
}

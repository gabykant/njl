<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Encore\Admin\Traits\AdminBuilder;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Encore\Admin\Auth\Database\AdminPermission;

class Gender extends Model implements AuthenticatableContract
{
    use Authenticatable, AdminBuilder, AdminPermission;

    protected $fillable = ['id', 'lb_name_gender', 'lb_is_delete'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $connection = config('admin.database.connection') ?: config('database.default');

        $this->setConnection($connection);

        $this->setTable(config('admin.database.gender_table'));

        parent::__construct($attributes);
    }
}

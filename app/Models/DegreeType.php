<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DegreeType extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'lb_degree_name',
        'lb_is_delete',
    ];

    protected $table = 'tbl_degree_type';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblCandidate extends Model
{
    protected $fillable = [
        'lb_user_id',
        'lb_gender_id',
        'lb_last_name',
        'lb_first_name',
        'lb_middle_name',
        'lb_dob',
        'lb_email',
        'lb_primary_phone',
        'lb_fax',
        'lb_facebook_url',
        'lb_twitter_url',
        'lb_linkedin_url',
        'lb_extra_activity',
        'lb_hobbies',
        'lb_photo_url',
        'lb_photo_full_portrait',
        'lb_graduate_date',
        'lb_program_studying_id',
        'lb_degree_type_id',
        'lb_start_date',
        'lb_end_date',
        'lb_job_type_id',
        'lb_position',
        'lb_department',
        'lb_company_name'
    ];
    
    protected $table = "tbl_candidate";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}

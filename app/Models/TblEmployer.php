<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEmployer extends Model
{
    protected $fillable = [
        'lb_user_id',
        'lb_job_type_id',
        'lb_city_id',
        'lb_state_id',
        'lb_email',
        'lb_phone_number',
        'lb_fax',
        'lb_facebook_url',
        'lb_twitter_url',
        'lb_linkedin_url',
        'lb_company_news',
        'lb_company_logo_path',
        'lb_industry_type_id',
        'lb_company_portrait',
        'lb_employee_firstname',
        'lb_employee_middlename',
        'lb_employee_lastname',
        'lb_company_create_date',
        'lb_hiring_status',
        'lb_position',
        'lb_department'
    ];
    
    protected $table = "tbl_employer";

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}

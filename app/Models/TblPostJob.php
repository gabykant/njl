<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPostJob extends Model
{
    protected $table = "tbl_job";
    
    public function jobType(){
        return $this->belongsTo('App\Models\JobType', 'lb_job_type_id');
    }
}

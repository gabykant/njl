<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblCurrency extends Model
{
    protected $table = 'tbl_currency';
}

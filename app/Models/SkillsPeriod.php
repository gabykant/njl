<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillsPeriod extends Model
{
    use SoftDeletes;

    protected $table = 'tbl_skills_period';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Encore\Admin\Traits\AdminBuilder;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Encore\Admin\Auth\Database\AdminPermission;

class Candidate extends Model implements AuthenticatableContract
{
    use Authenticatable, AdminBuilder, AdminPermission;

    protected $fillable = ['id', 'lb_user_id', 'lb_gender_id', 'lb_last_name', 'lb_first_name', 'lb_middle_name', 'lb_dob', 'lb_email', 'lb_primary_phone', 'lb_fax', 'lb_facebook_url', 'lb_twitter_url', 'lb_linkedin_url', 'lb_extra_activity', 'lb_hobbies', 'lb_photo_url', 'lb_graduate_date', 'lb_program_studying_id', 'lb_degree_type_id', 'lb_start_date', 'lb_end_date', 'lb_job_type_id', 'lb_position', 'lb_department', 'lb_company_name'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $connection = config('admin.database.connection') ?: config('database.default');

        $this->setConnection($connection);

        $this->setTable(config('admin.database.candidate_table'));

        parent::__construct($attributes);
    }
}

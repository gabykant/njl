<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $data['EmployeeName'] = $user->lb_first_name . ' ' . $user->lb_last_name;
        if($user->lb_role_id == 2){
            return view('home.employee_welcome', $data);
        }elseif ($user->lb_role_id == 3){
            return view('home.recruiter_welcome', $data);
        }else{
            if($request->isMethod('POST')){
                $q = $request->input("search_list");
                $all = DB::table("tbl_job AS J")
                    ->join("tbl_education AS E", "E.id", "=","J.lb_education_requirement")
                    ->join("tbl_job_category AS CAT", "CAT.id", "=", "J.lb_job_category_id")
                    ->join("tbl_postjob_rate AS JOBRATE", "JOBRATE.id", "=", "J.lb_payrate")
                    ->join("tbl_education AS PE", "PE.id", "=", "J.lb_pref_education_requirement")
                    ->join("tbl_recruiter AS RE", "RE.id", "=", "J.lb_recruiter_id")
                    ->join("tbl_cities AS C", "C.id", "=", "J.lb_city_id")
                    ->join("tbl_states AS S", "S.id", "=", "J.lb_state_id")
                    ->join("tbl_country AS CC", "CC.id", "=", "J.lb_country_id")
                    ->join("tbl_job_type AS TY", "TY.id", "=", "J.lb_job_type_id")
                    ->where("lb_title","LIKE", "%$q%")
                    ->select(["J.id AS JID", "J.lb_title as JTITLE", "J.lb_job_description AS JDESC", "TY.lb_job_type_name AS TYTYPENAME", "E.lb_education_name AS EEDUNAME", "C.lb_city_name AS CCITYNAME", "S.lb_state_name AS SSTATENAME", "CC.lb_country_name AS CCCOUNTRYNAME", "RE.lb_hiring_firstname AS RERECRUITERNAME", "J.created_at AS CREATEDAT"])->get();
            }else{
                $all = DB::table("tbl_job AS J")
                    ->join("tbl_education AS E", "E.id", "=","J.lb_education_requirement")
                    ->join("tbl_job_category AS CAT", "CAT.id", "=", "J.lb_job_category_id")
                    ->join("tbl_postjob_rate AS JOBRATE", "JOBRATE.id", "=", "J.lb_payrate")
                    ->join("tbl_education AS PE", "PE.id", "=", "J.lb_pref_education_requirement")
                    ->join("tbl_recruiter AS RE", "RE.id", "=", "J.lb_recruiter_id")
                    ->join("tbl_cities AS C", "C.id", "=", "J.lb_city_id")
                    ->join("tbl_states AS S", "S.id", "=", "J.lb_state_id")
                    ->join("tbl_country AS CC", "CC.id", "=", "J.lb_country_id")
                    ->join("tbl_job_type AS TY", "TY.id", "=", "J.lb_job_type_id")
                    ->select(["J.id AS JID", "J.lb_title as JTITLE", "J.lb_job_description AS JDESC", "TY.lb_job_type_name AS TYTYPENAME", "E.lb_education_name AS EEDUNAME", "C.lb_city_name AS CCITYNAME", "S.lb_state_name AS SSTATENAME", "CC.lb_country_name AS CCCOUNTRYNAME", "RE.lb_hiring_firstname AS RERECRUITERNAME", "J.created_at AS CREATEDAT"])->get();
            }
            $data['list_jobs'] = $all;
            return view('home', $data);
        }
    }
}

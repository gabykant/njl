<?php

namespace App\Http\Controllers;

use App\Models\JobCategory;
use App\Models\JobType;
use App\Models\DegreeType;
use App\Models\SkillsPeriod;
use App\Models\SalaryPayment;
use App\Models\PayFrequency;
use App\Models\TblCurrency;
use App\Models\TblCountry;
use App\Models\TblSkill;
use App\Models\PostJobRate;
use App\Models\TblPostJob;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Session;

class PostJobController extends Controller
{
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::forget('postJobSkills');
        $data = [];
        $data['categories'] = JobCategory::orderBy('lb_category_name')->pluck('lb_category_name', 'id');
        $data['types'] = JobType::orderBy('lb_job_type_name')->pluck('lb_job_type_name', 'id');
        $data['degrees'] = DegreeType::orderBy('lb_degree_name')->pluck('lb_degree_name', 'id');
        $data['skillsperiod'] = SkillsPeriod::orderBy('lb_skills_name')->pluck('lb_skills_name', 'id');
        $data['pay_rate_array'] = SalaryPayment::orderBy('lb_slot_name')->pluck('lb_slot_name', 'id');
        $data['pay_frequency'] = PayFrequency::orderBy('lb_pay_frequency_name')->pluck('lb_pay_frequency_name', 'id');
        $data['curency'] = TblCurrency::orderBy('lb_currency_name')->pluck('lb_short_name', 'id');
        $data['countries'] = TblCountry::where('lb_active', 1)->pluck('lb_country_name', 'id');
        $data['skills'] = TblSkill::where('lb_valide', 1)->pluck('lb_skill_name', 'id');

        return view('home.post_job', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $value = $request->session()->get('postJobSkills');
        $payrate_amount = $request->input('lb_payrate_amount');
        $currency_rate = (int) $request->input('lb_curency');
        $period_id = (int) $request->input('lb_period_id');
        
        $data = [
            'lb_job_category_id' => (int) $request->input('lb_job_category_id'),
            'lb_title' => $request->input('lb_title'),
            'lb_job_type_id' => (int) $request->input('lb_job_type_id'),
            'lb_education_requirement' => (int) $request->input('lb_education_requirement'),
            'lb_pref_education_requirement' => (int) $request->input('lb_pref_education_requirement'),
            'lb_job_description' => $request->input('lb_job_description'),
            'lb_payrate' => DB::table('tbl_postjob_rate')->insertGetId(['lb_amount_rate' => (double) $payrate_amount, 'lb_currency_id' => (int) $currency_rate, 'lb_period_id' => (int) $period_id]),
            'lb_recruiter_id' => Auth::user()->id,
            'lb_country_id' => (int) $request->input('lb_country_id'),
            'lb_state_id' => (int) $request->input('lb_state_id'),
            'lb_city_id' => (int) $request->input('lb_city_id')
        ];
        
        //Create the Job here
        $insert_new_jobpost = DB::table('tbl_job')
            ->insertGetId($data);
        
        if(is_null($value)){
            return redirect()->action('PostJobController@index');
        }
        //Insert into JobPostSkill with data from the SESSION
        foreach($value as $key=>$val){
            $data = json_decode($val);
            $insert_tbl_postjob_skills = DB::table('tbl_postjob_skills')->insert([
                'lb_post_job_id' => (int) $insert_new_jobpost,
                'lb_skill_id' => (int) $data->lb_skill_id,
                'lb_experience' => (int) $data->lb_experience,
                'lb_experience_period' => $data->lb_period_id
            ]);
        }
        echo "Data save successfully";
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function buildSession(Request $request){
        
        if($request->ajax()){
            $lb_skill_name_id = (int) $request->input('lb_skill_name');
            $lb_skillperiod_id = (int) $request->input('lb_skillsperiod_id');
            $array_to_json = json_encode([
                'lb_skill_id' => $lb_skill_name_id,
                'lb_skill_name' => TblSkill::where('id', $lb_skill_name_id)->first()->lb_skill_name,
                'lb_experience' => (int) $request->input('lb_experience'),
                'lb_period_id' => (int) $lb_skillperiod_id,
                'lb_skillsperiod_id' => SkillsPeriod::where('id', $lb_skillperiod_id)->first()->lb_skills_name
            ]);
            $request->session()->push('postJobSkills', $array_to_json);
            $value = $request->session()->get('postJobSkills');
            echo json_encode($value);
        }
    }
    
    public function getAll(Request $request){
        if($request->ajax()){
            $all = DB::table("tbl_job AS J")
                ->join("tbl_education AS E", "E.id", "=","J.lb_education_requirement")
                ->join("tbl_job_category AS CAT", "CAT.id", "=", "J.lb_job_category_id")
                ->join("tbl_postjob_rate AS JOBRATE", "JOBRATE.id", "=", "J.lb_payrate")
                ->join("tbl_education AS PE", "PE.id", "=", "J.lb_pref_education_requirement")
                ->join("tbl_recruiter AS RE", "RE.id", "=", "J.lb_recruiter_id")
                ->join("tbl_cities AS C", "C.id", "=", "J.lb_city_id")
                ->join("tbl_states AS S", "S.id", "=", "J.lb_state_id")
                ->join("tbl_country AS CC", "CC.id", "=", "J.lb_country_id")
                ->join("tbl_job_type AS TY", "TY.id", "=", "J.lb_job_type_id")->get();
        }
        return json_encode($all);
    }

    public function passed(){
        $lb_recruiter_id = Auth::user()->id;
        $data['list_jobs'] = DB::table('tbl_job AS J')
        ->join('tbl_job_type as TY', 'TY.id', '=', 'J.lb_job_type_id')
        ->select('J.created_at as JCREATED', 'J.lb_title as JTITLE', 'TY.lb_job_type_name as TYNAME', 'J.id as JID')
        ->where(['lb_recruiter_id'=>$lb_recruiter_id])->get();
        return view('home.recruiter_pass_job', $data);
    }
}

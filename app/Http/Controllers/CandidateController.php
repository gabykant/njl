<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\JobType;
use App\Models\DegreeType;
use App\Models\ProgramStudying;
use App\Models\TblMajorProgram;
use App\Models\TblGrade;
use App\Models\TblEducation;
use App\Models\TblCountry;
use App\Models\TblCertificateType;
use App\Models\TblTraining;
use App\Models\Gender;
use Validator;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function lastApplied(){
        $candidate_id = Auth::user()->id;
        if(!$candidate_id) exit;
        $data['list_jobs'] = DB::table("tbl_job_apply AS AJ")
            //->join("tbl_job AS A", "AJ.lb_job_id", "=", "A.id")
            ->where("AJ.lb_candidate_id", "=", $candidate_id)->get();
        return view('home.candidates_applied', $data);
    }
    
    /*public function applyto(Request $request, $j){
        if($j==0 || is_null($j)){
            abort(403, "The page you're looking for is not reachable");
        }
        $job_id = $j;
        $all = DB::table("tbl_job AS J")
                ->join("tbl_education AS E", "E.id", "=","J.lb_education_requirement")
                ->join("tbl_job_category AS CAT", "CAT.id", "=", "J.lb_job_category_id")
                ->join("tbl_postjob_rate AS JOBRATE", "JOBRATE.id", "=", "J.lb_payrate")
                ->join("tbl_education AS PE", "PE.id", "=", "J.lb_pref_education_requirement")
                ->join("tbl_recruiter AS RE", "RE.id", "=", "J.lb_recruiter_id")
                ->join("tbl_cities AS C", "C.id", "=", "J.lb_city_id")
                ->join("tbl_states AS S", "S.id", "=", "J.lb_state_id")
                ->join("tbl_country AS CC", "CC.id", "=", "J.lb_country_id")
                ->join("tbl_job_type AS TY", "TY.id", "=", "J.lb_job_type_id")
                ->where("J.id", "=", (int)$job_id)->get();
        foreach($all as $a){
            var_dump($a);
        }
        //var_dump($all);
        return view('jobs.details', $all);
    }*/
    
    public function applyto(Request $request, $j){
        if($j==0 || is_null($j)){
            abort(403, "The page you're looking for is not reachable");
        }
        $job_id = $j;
        
        if($request->isMethod('post')){
            $job_id = $request->input('lb_job_id');
            $user_id = Auth::user()->id;
            
            $saveApply = DB::table('tbl_job_apply')->insert(['lb_job_id'=>$job_id, 'lb_candidate_id' => $user_id]);
            $request->session()->flash('save_apply_job', 'Congratulations !! You apply to this job position successfully');
        }
        
        $check_applied = DB::table('tbl_job_apply')->where([['lb_candidate_id', '=', Auth::user()->id],['lb_job_id', '=', $job_id]])->get();
        
        $all = DB::table("tbl_job AS J")
                ->join("tbl_education AS E", "E.id", "=","J.lb_education_requirement")
                ->join("tbl_job_category AS CAT", "CAT.id", "=", "J.lb_job_category_id")
                ->join("tbl_postjob_rate AS JOBRATE", "JOBRATE.id", "=", "J.lb_payrate")
                ->join("tbl_education AS PE", "PE.id", "=", "J.lb_pref_education_requirement")
                ->join("tbl_recruiter AS RE", "RE.id", "=", "J.lb_recruiter_id")
                ->join("tbl_cities AS C", "C.id", "=", "J.lb_city_id")
                ->join("tbl_states AS S", "S.id", "=", "J.lb_state_id")
                ->join("tbl_country AS CC", "CC.id", "=", "J.lb_country_id")
                ->join("tbl_job_type AS TY", "TY.id", "=", "J.lb_job_type_id")
                ->where("J.id", "=", (int)$job_id)
                ->select(["J.id AS JID", "J.lb_title as JTITLE", "J.lb_job_description AS JDESC", "TY.lb_job_type_name AS TYTYPENAME", "E.lb_education_name AS EEDUNAME", "C.lb_city_name AS CCITYNAME", "S.lb_state_name AS SSTATENAME", "CC.lb_country_name AS CCCOUNTRYNAME", "RE.lb_hiring_firstname AS RERECRUITERNAME", "J.created_at AS CREATEDAT"])
                ->limit(1)->get();
        $a = $all[0];
        $data = array(
            'id' => $a->JID,
            'title' => $a->JTITLE,
            'description' => $a->JDESC,
            'type' => $a->TYTYPENAME,
            'education' => $a->EEDUNAME,
            'city' => $a->CCITYNAME,
            'state' => $a->SSTATENAME,
            'country' => $a->CCCOUNTRYNAME,
            'recruiter' => $a->RERECRUITERNAME,
            'created_on' => $a->CREATEDAT
        );
        $data['checked'] = count($check_applied);
        return view('jobs.details', $data);
    }
    
    public function personalinfo(Request $request){
        if($request->isMethod('post')){
            $data = array(
                'lb_first_name' => $request->input('lb_first_name'),
                'lb_last_name' => $request->input('lb_last_name'),
                'lb_middle_name' => $request->input('lb_middle_name')
            );

            if(!$request->hasFile('lb_photo_url') || !$request->hasFile('lb_photo_full_portrait')){
                return redirect()->action('CandidateController@personalinfo');
            }
            if( !$request->file('lb_photo_url')->isValid()){
                return redirect()->action('CandidateController@personalinfo');
            }
            if( !$request->file('lb_photo_full_portrait')->isValid()){
                return redirect()->action('CandidateController@personalinfo');
            }
            $path_full_portrait = $request->lb_photo_full_portrait->storeAs('registers/candidates', 'portait_' . time() .'_'. Auth::user()->id.'.jpg');
            $path_photo = $request->lb_photo_url->storeAs('registers/candidates', 'photo_' . time() .'_'. Auth::user()->id.'.jpg');
            
            $userInformation = array(
                'lb_gender_id' => $request->input('lb_gender_id'),
                'lb_dob' => $request->input('lb_dob'),
                'lb_primary_phone' => $request->input('lb_primary_phone'),
                'lb_fax' => $request->input('lb_fax'),
                'lb_facebook_url' => $request->input('lb_facebook_url'),
                'lb_twitter_url' => $request->input('lb_twitter_url'),
                'lb_linkedin_url' => $request->input('lb_linkedin_url'),
                'lb_extra_activity' => $request->input('lb_extra_activity'),
                'lb_hobbies' => $request->input('lb_hobbies'),
                'lb_photo_url' => $path_photo,
                'lb_photo_full_portrait' => $path_full_portrait
            );
        
            $email = Auth::user()->email;
            $update = DB::table('users')->where([['id', '=', Auth::user()->id], ['email', '=', $email]])->update($data);

            $update_education = DB::table('tbl_candidate')->where([['lb_user_id', '=', Auth::user()->id], ['lb_email', '=', $email]])->update($userInformation);
            //return redirect()->action('CandidateController@personalinfo');
        }
        $userInformation = DB::table('tbl_candidate')->where(
            [
                ['lb_user_id', '=', Auth::user()->id],
                ['lb_email', '=', Auth::user()->email]
            ])->get();
        
        if(count($userInformation) <= 0){
            $in = DB::table('tbl_candidate')->insert(
                [
                    'lb_user_id' => Auth::user()->id,
                    'lb_first_name' => Auth::user()->lb_first_name,
                    'lb_last_name' => Auth::user()->lb_last_name,
                    'lb_middle_name' => Auth::user()->lb_middle_name,
                    'lb_email' => Auth::user()->email
                ]
            );
        }
        $data['userInformation'] = DB::table('tbl_candidate')->where(
            [
                ['lb_user_id', '=', Auth::user()->id],
                ['lb_email', '=', Auth::user()->email]
            ])->get();
        
        $data['gender'] = Gender::pluck('lb_name_gender', 'id');
        return view('home.basic', $data);
    }
    
    public function candidate_education(Request $request){
        if($request->isMethod('post')){
            
        }
        $data['list_edu'] = DB::table('tbl_education_history AS EH')
        ->join('tbl_major_program AS MP', 'EH.lb_study_field_id', '=', 'MP.id')->get();
        return view('home.candidate_education_list', $data);
    }
    
    public function candidate_education_add(Request $request){
        if($request->isMethod('post')){
            $validate = Validator::make($request->all(), [
                'lb_school_country' => 'required|not_in:0',
                'lb_school_state' => 'required|not_in:0',
                'lb_school_city' => 'required|not_in:0',
                'lb_name' => 'required',
                'lb_study_field_id' => 'required|not_in:0',
                'lb_degree_type_id' => 'required|not_in:0',
                'lb_student_type_id'=> 'required|not_in:0',
                'lb_grade_id'=> 'required|not_in:0'
            ])->validate();

            $candidate_id = Auth::user()->id;
            $data = $request->all();
            unset($data['_token']);
            $data['lb_candidate_id'] = $candidate_id;
            $return = DB::table('tbl_education_history')->insert($data);
            if($return){
                return redirect()->action('CandidateController@candidate_education');
            }
            /*$data = array(
                'lb_school_country' => 
            );*/
        }
        $array_countries = [0=>'Choose from the list'];
        //$data['countries'] = TblCountry::where('lb_active', 1)->pluck('lb_country_name', 'id');
        $countries=DB::select("select id, lb_country_name from tbl_country where lb_active = 1");
        for($i=0; $i<count($countries); $i++){
            $array_countries[$countries[$i]->id] = $countries[$i]->lb_country_name;
        }
        $data['countries']=$array_countries;
        $data['grades']=TblGrade::pluck('lb_grade_name', 'id');
        $data['studentType']= TblEducation::pluck('lb_education_name', 'id');
        $data['types']=JobType::pluck('lb_job_type_name', 'id');
        $data['degree']=DegreeType::pluck('lb_degree_name', 'id');
        $data['fields']=TblMajorProgram::pluck('lb_major_name', 'id');
        
        return view('home.candidate_education_add', $data);
    }

    public function candidate_training(Request $request){
        if($request->isMethod('post')){
            /*$candidate_id = Auth::user()->id;
            $data = array(
                'lb_candidate_id'=> $candidate_id,
                'lb_program_name'=>$request->input('lb_program_name'),
                'lb_program_length'=>$request->input('lb_program_length'),
                'lb_program_length_drop'=>$request->input('lb_program_length_drop'),
                'lb_attended_from'=>$request->input('lb_attended_from'),
                'lb_attended_to'=>$request->input('lb_attended_to'),
                'lb_graduated'=>$request->input('lb_graduated'),
                'lb_provider_name'=>$request->input('lb_provider_name'),
                'lb_training_location'=>$request->input('lb_training_location'),
                'lb_training_website' =>$request->input('lb_training_website'),
                'lb_training_address'=>$request->input('lb_training_address'),
                'lb_school_country'=>$request->input('lb_school_country'),
                'lb_school_state' => $request->input('lb_school_state'),
                'lb_school_city'=>$request->input('lb_school_city'),
                'lb_zip_code'=>$request->input('lb_zip_code'),
                'lb_training_address' =>$request->input('lb_training_address')
            );
            $validate = Validator::make($request->all(),[
                'lb_program_length' => 'required',
                'lb_provider_name' => 'required',
                'lb_provider_name' => 'required'
            ])->validate();
            
            $return = DB::table('tbl_professional_training')->insert($data);
            if(!$return){
                return redirect()->action(candidate_training_add);
            }*/
        }
        $data['list_edu'] = TblTraining::all();
        
        return view('home.candidate_training_list', $data);
    }

    public function candidate_training_add(Request $request){
        if($request->isMethod('post')){
            $candidate_id = Auth::user()->id;
            $data = array(
                'lb_candidate_id'=> $candidate_id,
                'lb_program_name'=>$request->input('lb_program_name'),
                'lb_program_length'=>$request->input('lb_program_length'),
                'lb_program_length_drop'=>$request->input('lb_program_length_drop'),
                'lb_attended_from'=>$request->input('lb_attended_from'),
                'lb_attended_to'=>$request->input('lb_attended_to'),
                'lb_graduated'=>$request->input('lb_graduated'),
                'lb_provider_name'=>$request->input('lb_provider_name'),
                'lb_training_location'=>$request->input('lb_training_location'),
                'lb_training_website' =>$request->input('lb_training_website'),
                'lb_training_address'=>$request->input('lb_training_address'),
                'lb_school_country'=>$request->input('lb_school_country'),
                'lb_school_state' => $request->input('lb_school_state'),
                'lb_school_city'=>$request->input('lb_school_city'),
                'lb_zip_code'=>$request->input('lb_zip_code'),
                'lb_training_address' =>$request->input('lb_training_address')
            );
            $validate = Validator::make($request->all(), [
                'lb_program_length' => 'required',
                'lb_provider_name' => 'required',
            ])->validate();
            //var_dump($data);exit;
            $return = DB::table('tbl_professional_training')->insert($data);
            if($return){
                return redirect()->action('CandidateController@candidate_training');
            }
        }
        /*$data['list_edu'] = array();
        return view('home.candidate_training_list', $data);*/
        $array_countries = [0=>'Choose from the list'];
        $data['certificates'] = TblCertificateType::pluck('lb_certificate_name', 'id');
        //$countries = TblCountry::where('lb_active', 1)->pluck('lb_country_name', 'id');//->prepend('Choose from the list');
        $countries=DB::select("select id, lb_country_name from tbl_country where lb_active = 1");
        for($i=0; $i<count($countries); $i++){
            $array_countries[$countries[$i]->id] = $countries[$i]->lb_country_name;
        }
        $data['countries']=$array_countries;
        //$date['periods'] = SkillsPeriod::pluck('lb_skill_name', 'id');
        return view('home.candidate_training_add', $data);
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\RegisterController;
use App\Models\TblUserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

/**
 * Description of CandidateRegisterController
 *
 * @author gabykant
 */
class CandidateRegisterController extends RegisterController{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.register_candidate');
    }
    
    protected function validator(array $data) {
        return Validator::make($data, [
            'lb_first_name' => 'required|string|max:255',
            'lb_last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    
    protected function create(array $data) {
        return User::create([
            'lb_role_id' => TblUserRole::where('lb_role_name', '=', 'candidate')->first()->id,
            'lb_first_name' => $data['lb_first_name'],
            'lb_middle_name' => $data['lb_middle_name'],
            'lb_last_name' => $data['lb_last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function register(Request $request){
        $data = $request->input();
        $this->validator($data)->validate();
        $user = $this->create($data);
        if(is_null($user)){
            $request->session()->flash('RegistrationError', 'A problem occurs while registring this account');
            return view('home.register_candidate');
        }else{
            $input = array(
                'lb_user_id' => $user->id,
                'lb_first_name' => $user->lb_first_name,
                'lb_middle_name' => $user->lb_middle_name,
                'lb_last_name' => $user->lb_last_name,
                'lb_email' => $user->email,
            );
            $profile = DB::table('tbl_candidate')->insert($input);
        }
        return view('home.register_confirmation', ['email' => $data['email']]);
    }
}

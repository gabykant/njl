<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TblPostJob;

class FindJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allJobs = TblPostJob::join('tbl_job_type', 'tbl_job.lb_job_type_id', '=', 'tbl_job_type.id')
            ->join('tbl_job_category as CAT', 'tbl_job.lb_job_category_id', '=', 'CAT.id')
            ->join('tbl_degree_type as DT', 'tbl_job.lb_education_requirement', '=', 'DT.id')
            ->join('tbl_country as C', 'tbl_job.lb_country_id', '=', 'C.id')
            //->join('tbl_states as S', 'tbl_job.lb_state_id', '=', 'S.id')
            ->get();
        $data['allJobs'] = $allJobs;
        return view('home.jobslist', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

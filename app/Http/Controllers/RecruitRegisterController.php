<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\RegisterController;
use App\Models\TblUserRole;
use App\User;

/**
 * Description of RecruitRegisterController
 *
 * @author gabykant
 */
class RecruitRegisterController extends RegisterController{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.register_recruiter');
    }

    protected function validator(array $data) {
        return Validator::make($data, [
            'lb_hiring_firstname' => 'required|string|max:255',
            'lb_hiring_lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    
    protected function create(array $data) {
        return User::create([
            'lb_role_id' => TblUserRole::where('lb_role_name', '=', 'recruiter')->first()->id,
            'lb_first_name' => $data['lb_hiring_firstname'],
            'lb_middle_name' => $data['lb_hiring_middlename'],
            'lb_last_name' => $data['lb_hiring_lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function register(Request $request){
        $data = $request->input();
        $this->validator($data)->validate();
        $user = $this->create($data);
        if(is_null($user)){
            $request->session()->flash('RegistrationError', 'A problem occurs while registring this account');
            return view('home.register_recruiter');
        }
        return view('home.register_confirmation', ['email' => $data['email']]);
    }
}

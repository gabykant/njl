<?php

namespace App\Http\Controllers\Auth;

use App\Models\TblCandidate;
use App\Models\TblEmployer;
use App\Models\TblRecruiter;
use App\Models\TblUserRole;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if($data['profile'] === 'candidate'){
            $validator = Validator::make($data, [
                'lb_first_name' => 'required|string|max:255',
                'lb_last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }elseif ($data['profile'] === 'recruiter'){
            $validator = Validator::make($data, [
                'lb_hiring_firstname' => 'required|string|max:255',
                'lb_hiring_lastname' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }else{
            $validator = Validator::make($data, [
                'lb_employee_firstname' => 'required|string|max:255',
                'lb_employee_lastname' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);
        }

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if($data['profile'] === 'candidate') {
            $user = User::create([
                'lb_role_id' => TblUserRole::where('lb_role_name', '=', 'candidate')->get()->id,
                'lb_first_name' => $data['lb_first_name'],
                'lb_middle_name' => $data['lb_middle_name'],
                'lb_last_name' => $data['lb_last_name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            $this->fillcandidate($data, $user->id);
        }elseif($data['profile'] === 'recruiter'){
            //echo TblUserRole::where('lb_role_name', '=', 'recruiter')->first()->id; exit;
            //var_dump($data);exit;
            $user = User::create([
                'lb_role_id' => TblUserRole::where('lb_role_name', '=', 'recruiter')->first()->id,
                'lb_first_name' => $data['lb_hiring_firstname'],
                'lb_middle_name' => $data['lb_hiring_middlename'],
                'lb_last_name' => $data['lb_hiring_lastname'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            //var_dump($user);exit;
            //echo $user->id;
            $this->fillRecruiter($data, $user->id);
        }elseif ($data['profile'] === 'employer'){
            $user = User::create([
                'lb_role_id' => TblUserRole::where('lb_role_name', '=', 'employer')->get()->id,
                'lb_first_name' => $data['lb_employee_firstname'],
                'lb_middle_name' => $data['lb_employee_middlename'],
                'lb_last_name' => $data['lb_employee_lastname'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            $this->fillEmployer($data, $user->id);
        }

        //Mail::to($user->email)->send(new ConfirmRegistration($user));

        return $user;
    }

    private function fillcandidate($data, $user_id){
        TblCandidate::create([
            'lb_user_id' => $user_id,
            'lb_first_name'  => $data['lb_first_name'],
            'lb_middle_name' => $data['lb_middle_name'],
            'lb_last_name' => $data['lb_last_name'],
            'lb_email' => $data['email'],
        ]);
    }

    private function fillRecruiter($data, $user_id){
        TblRecruiter::create([
            'lb_user_id' => $user_id,
            'lb_hiring_firstname' => $data['lb_hiring_firstname'],
            'lb_hiring_middlename' => $data['lb_hiring_middlename'],
            'lb_hiring_lastname' => $data['lb_hiring_lastname'],
            'lb_email' => $data['email'],
        ]);
    }

    private function fillEmployer($data, $user_id){
        TblEmployer::create([
            'lb_user_id' => $user_id,
            'lb_employee_firstname' => $data['lb_employee_firstname'],
            'lb_employee_middlename' => $data['lb_employee_middlename'],
            'lb_employee_lastname' => $data['lb_employee_lastname'],
            'lb_email' => $data['email'],
        ]);
    }

    private function getValue($label){
        $array = [
            'candidate' => TblCandidate::class,
            'recruiter' => TblRecruiter::class,
            'employer' => TblEmployer::class
        ];

        return $array[$label];
    }
    
    public function change_password(){
        
    }
}

-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: njl
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` VALUES (1,0,1,'Index','fa-bar-chart','/',NULL,NULL),(2,0,2,'Admin','fa-tasks','',NULL,NULL),(3,2,3,'Users','fa-users','auth/users',NULL,NULL),(4,2,4,'Roles','fa-user','auth/roles',NULL,NULL),(5,2,5,'Permission','fa-user','auth/permissions',NULL,NULL),(6,2,6,'Menu','fa-bars','auth/menu',NULL,NULL),(7,2,7,'Operation log','fa-history','auth/logs',NULL,NULL),(8,0,8,'Helpers','fa-gears','',NULL,NULL),(9,8,9,'Scaffold','fa-keyboard-o','helpers/scaffold',NULL,NULL),(10,8,10,'Database terminal','fa-database','helpers/terminal/database',NULL,NULL),(11,8,11,'Laravel artisan','fa-terminal','helpers/terminal/artisan',NULL,NULL),(12,0,12,'Post a job','fa-graduation-cap','new/post','2017-06-12 04:41:56','2017-06-12 04:42:17'),(13,0,13,'Candidates','fa-bars','candidates','2017-06-13 20:32:22','2017-06-27 11:16:39'),(14,0,14,'Job Categories','fa-bars','/categories','2017-06-26 15:35:41','2017-06-27 11:16:39'),(15,0,15,'Degree type','fa-bars','/degrees','2017-06-27 11:11:22','2017-06-27 11:16:39'),(16,0,0,'Pay Rate Schedule','fa-bars','paymentslot','2017-06-27 11:32:21','2017-06-27 16:22:14'),(17,0,0,'Skills Period','fa-bars','skillsperiod','2017-06-27 11:44:34','2017-06-27 11:44:34'),(18,0,0,'Pay Frequency','fa-bars','/payfrequency','2017-06-27 15:54:25','2017-06-27 15:54:25'),(19,0,0,'Geo Datasource','fa-bars','#','2017-06-27 18:13:47','2017-06-27 18:13:47'),(20,19,0,'Countries','fa-bars','/countries','2017-06-27 18:14:27','2017-06-27 18:14:27'),(21,19,0,'States','fa-bars','/states','2017-06-27 18:15:07','2017-06-27 18:15:07'),(22,19,0,'Cities','fa-bars','/cities','2017-06-27 18:15:29','2017-06-27 18:15:29'),(23,0,0,'Job Type','fa-bars','/jobtypes','2017-06-27 23:35:32','2017-06-27 23:35:32');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_operation_log`
--

LOCK TABLES `admin_operation_log` WRITE;
/*!40000 ALTER TABLE `admin_operation_log` DISABLE KEYS */;
INSERT INTO `admin_operation_log` VALUES (1,1,'admin','GET','192.168.10.1','[]','2017-06-12 04:29:43','2017-06-12 04:29:43'),(2,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:30:25','2017-06-12 04:30:25'),(3,1,'admin/auth/permissions','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:30:44','2017-06-12 04:30:44'),(4,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:31:49','2017-06-12 04:31:49'),(5,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:02','2017-06-12 04:32:02'),(6,1,'admin/auth/roles','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:33','2017-06-12 04:32:33'),(7,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:39','2017-06-12 04:32:39'),(8,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:32:52','2017-06-12 04:32:52'),(9,1,'admin/auth/permissions','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:00','2017-06-12 04:33:00'),(10,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:06','2017-06-12 04:33:06'),(11,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"candidate_permission\",\"name\":\"CANDIDATE PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:33:37','2017-06-12 04:33:37'),(12,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:33:38','2017-06-12 04:33:38'),(13,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:33:45','2017-06-12 04:33:45'),(14,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"recruiter_permission\",\"name\":\"RECRUITER PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:34:14','2017-06-12 04:34:14'),(15,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:34:15','2017-06-12 04:34:15'),(16,1,'admin/auth/permissions/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:19','2017-06-12 04:34:19'),(17,1,'admin/auth/permissions','POST','192.168.10.1','{\"slug\":\"employer_permission\",\"name\":\"EMPLOYER PERMISSION\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/permissions\"}','2017-06-12 04:34:47','2017-06-12 04:34:47'),(18,1,'admin/auth/permissions','GET','192.168.10.1','[]','2017-06-12 04:34:48','2017-06-12 04:34:48'),(19,1,'admin/auth/roles','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:51','2017-06-12 04:34:51'),(20,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:34:55','2017-06-12 04:34:55'),(21,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"candidate\",\"name\":\"CANDIDATE\",\"permissions\":[\"1\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:35:39','2017-06-12 04:35:39'),(22,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:35:40','2017-06-12 04:35:40'),(23,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:35:48','2017-06-12 04:35:48'),(24,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"recruiter\",\"name\":\"RECRUITER\",\"permissions\":[\"2\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:36:06','2017-06-12 04:36:06'),(25,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:36:06','2017-06-12 04:36:06'),(26,1,'admin/auth/roles/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:10','2017-06-12 04:36:10'),(27,1,'admin/auth/roles','POST','192.168.10.1','{\"slug\":\"employer\",\"name\":\"EMPLOYER\",\"permissions\":[\"3\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/roles\"}','2017-06-12 04:36:24','2017-06-12 04:36:24'),(28,1,'admin/auth/roles','GET','192.168.10.1','[]','2017-06-12 04:36:26','2017-06-12 04:36:26'),(29,1,'admin/auth/users','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:36','2017-06-12 04:36:36'),(30,1,'admin/auth/users/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:36:42','2017-06-12 04:36:42'),(31,1,'admin/auth/users','POST','192.168.10.1','{\"username\":\"gabykant\",\"name\":\"Kwaye Kant\",\"password\":\"computer\",\"password_confirmation\":\"computer\",\"roles\":[\"2\",null],\"permissions\":[\"1\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/users\"}','2017-06-12 04:38:57','2017-06-12 04:38:57'),(32,1,'admin/auth/users','GET','192.168.10.1','[]','2017-06-12 04:38:59','2017-06-12 04:38:59'),(33,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:39:17','2017-06-12 04:39:17'),(34,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Post a job\",\"icon\":\"fa-graduation-cap\",\"uri\":\"new\\/post\",\"roles\":[\"3\",\"4\",null],\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\"}','2017-06-12 04:41:55','2017-06-12 04:41:55'),(35,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-12 04:41:57','2017-06-12 04:41:57'),(36,1,'admin/auth/menu','POST','192.168.10.1','{\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":12}]\"}','2017-06-12 04:42:17','2017-06-12 04:42:17'),(37,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:42:18','2017-06-12 04:42:18'),(38,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:45:41','2017-06-12 04:45:41'),(39,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/menu\"}','2017-06-12 04:48:05','2017-06-12 04:48:05'),(40,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:48:07','2017-06-12 04:48:07'),(41,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\"}','2017-06-12 04:49:04','2017-06-12 04:49:04'),(42,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:49:05','2017-06-12 04:49:05'),(43,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\"}','2017-06-12 04:53:37','2017-06-12 04:53:37'),(44,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:53:38','2017-06-12 04:53:38'),(45,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:53:48','2017-06-12 04:53:48'),(46,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:54:28','2017-06-12 04:54:28'),(47,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:02','2017-06-12 04:55:02'),(48,1,'admin/auth/setting','PUT','192.168.10.1','{\"name\":\"Kwaye Kant\",\"password\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"password_confirmation\":\"$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy\",\"_token\":\"Wgbrfc2heaGLWBCYplmXwVFBrPdU2CnDzP5H3FWs\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/login\"}','2017-06-12 04:55:36','2017-06-12 04:55:36'),(49,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:38','2017-06-12 04:55:38'),(50,1,'admin/auth/setting','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:55:45','2017-06-12 04:55:45'),(51,1,'admin/auth/setting','GET','192.168.10.1','[]','2017-06-12 04:55:50','2017-06-12 04:55:50'),(52,1,'admin','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 04:55:58','2017-06-12 04:55:58'),(53,1,'admin','GET','192.168.10.1','[]','2017-06-12 09:59:31','2017-06-12 09:59:31'),(54,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 09:59:53','2017-06-12 09:59:53'),(55,1,'admin','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-12 10:00:02','2017-06-12 10:00:02'),(56,1,'admin','GET','192.168.10.1','[]','2017-06-13 09:07:07','2017-06-13 09:07:07'),(57,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-13 20:31:49','2017-06-13 20:31:49'),(58,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Candidates\",\"icon\":\"fa-bars\",\"uri\":\"candidate\\/list\",\"roles\":[\"1\",null],\"_token\":\"cZF7wfX8z8TTW4iOE9adXro5IvoI383oclqnQfUo\"}','2017-06-13 20:32:21','2017-06-13 20:32:21'),(59,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-13 20:32:23','2017-06-13 20:32:23'),(60,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-13 20:32:44','2017-06-13 20:32:44'),(61,1,'admin/auth/users','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-13 21:37:18','2017-06-13 21:37:18'),(62,1,'admin/auth/users/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-13 21:37:29','2017-06-13 21:37:29'),(63,1,'admin/auth/users/create','GET','192.168.10.1','[]','2017-06-14 11:19:57','2017-06-14 11:19:57'),(64,1,'admin/candidate/list','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 11:20:05','2017-06-14 11:20:05'),(65,1,'admin/auth/login','GET','192.168.10.1','[]','2017-06-14 11:21:14','2017-06-14 11:21:14'),(66,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 11:21:19','2017-06-14 11:21:19'),(67,1,'admin/auth/menu/13/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 11:21:25','2017-06-14 11:21:25'),(68,1,'admin/auth/menu/13','PUT','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Candidates\",\"icon\":\"fa-bars\",\"uri\":\"candidates\",\"roles\":[\"1\",null],\"_token\":\"kFiZ8narGSmC1RuXwL4XgAAo3k5BVbWvUDPmRj3G\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/menu\"}','2017-06-14 11:21:32','2017-06-14 11:21:32'),(69,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-14 11:21:33','2017-06-14 11:21:33'),(70,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-14 11:21:42','2017-06-14 11:21:42'),(71,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 11:21:45','2017-06-14 11:21:45'),(72,1,'admin/candidates','GET','192.168.10.1','[]','2017-06-14 11:23:38','2017-06-14 11:23:38'),(73,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 11:23:43','2017-06-14 11:23:43'),(74,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 11:24:17','2017-06-14 11:24:17'),(75,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 11:29:42','2017-06-14 11:29:42'),(76,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 11:59:32','2017-06-14 11:59:32'),(77,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 11:59:37','2017-06-14 11:59:37'),(78,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:01:50','2017-06-14 12:01:50'),(79,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:01:54','2017-06-14 12:01:54'),(80,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:03:42','2017-06-14 12:03:42'),(81,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:03:46','2017-06-14 12:03:46'),(82,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:06:01','2017-06-14 12:06:01'),(83,1,'admin/candidates','GET','192.168.10.1','[]','2017-06-14 12:08:02','2017-06-14 12:08:02'),(84,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:08:07','2017-06-14 12:08:07'),(85,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:16:59','2017-06-14 12:16:59'),(86,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:18:51','2017-06-14 12:18:51'),(87,1,'admin/api/genders','GET','192.168.10.1','[]','2017-06-14 12:20:39','2017-06-14 12:20:39'),(88,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:21:03','2017-06-14 12:21:03'),(89,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:22:15','2017-06-14 12:22:15'),(90,1,'admin/api/genders','GET','192.168.10.1','{\"q\":\"M\"}','2017-06-14 12:22:20','2017-06-14 12:22:20'),(91,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:28:18','2017-06-14 12:28:18'),(92,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:32:04','2017-06-14 12:32:04'),(93,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 12:33:50','2017-06-14 12:33:50'),(94,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:37:49','2017-06-14 12:37:49'),(95,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 12:55:43','2017-06-14 12:55:43'),(96,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 13:01:07','2017-06-14 13:01:07'),(97,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 13:02:42','2017-06-14 13:02:42'),(98,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-14 13:09:52','2017-06-14 13:09:52'),(99,1,'admin/api/genders','GET','192.168.10.1','{\"q\":\"M\"}','2017-06-14 13:11:05','2017-06-14 13:11:05'),(100,1,'admin/auth/users','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 14:38:18','2017-06-14 14:38:18'),(101,1,'admin/auth/users/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-14 14:38:23','2017-06-14 14:38:23'),(102,1,'admin/auth/users/create','GET','192.168.10.1','[]','2017-06-14 14:43:30','2017-06-14 14:43:30'),(103,1,'admin/auth/users/create','GET','192.168.10.1','[]','2017-06-14 18:11:22','2017-06-14 18:11:22'),(104,1,'admin','GET','192.168.10.1','[]','2017-06-15 08:12:39','2017-06-15 08:12:39'),(105,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-15 08:12:44','2017-06-15 08:12:44'),(106,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_program_studying\",\"model_name\":\"App\\\\Models\\\\ProgramStudying\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\ProgramStudyingController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"integer\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"lb_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_is_delete\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"6w4J9QC44XEs0x29UXg8x45O7WOvQ1fZsR9XYfXN\"}','2017-06-15 08:16:34','2017-06-15 08:16:34'),(107,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-15 08:16:37','2017-06-15 08:16:37'),(108,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_program_studying\",\"model_name\":\"App\\\\Models\\\\ProgramStudying\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\ProgramStudyingController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"id\",\"type\":\"integer\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"lb_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_is_delete\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"6w4J9QC44XEs0x29UXg8x45O7WOvQ1fZsR9XYfXN\"}','2017-06-15 08:17:13','2017-06-15 08:17:13'),(109,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-15 08:17:15','2017-06-15 08:17:15'),(110,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_program_studying\",\"model_name\":\"App\\\\Models\\\\ProgramStudying\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\ProgramStudyingController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":{\"1\":{\"name\":\"lb_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},\"2\":{\"name\":\"lb_is_delete\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":null}},\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"6w4J9QC44XEs0x29UXg8x45O7WOvQ1fZsR9XYfXN\"}','2017-06-15 08:17:25','2017-06-15 08:17:25'),(111,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-15 08:17:27','2017-06-15 08:17:27'),(112,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_program_studying\",\"model_name\":\"App\\\\Models\\\\ProgramStudying\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\ProgramStudyingController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":{\"1\":{\"name\":\"lb_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},\"2\":{\"name\":\"lb_is_delete\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":null}},\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"6w4J9QC44XEs0x29UXg8x45O7WOvQ1fZsR9XYfXN\"}','2017-06-15 08:18:53','2017-06-15 08:18:53'),(113,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-15 08:18:55','2017-06-15 08:18:55'),(114,1,'admin/helpers/terminal/database','GET','192.168.10.1','[]','2017-06-15 10:40:57','2017-06-15 10:40:57'),(115,1,'admin/helpers/terminal/database','POST','192.168.10.1','{\"c\":\"db:mysql\",\"q\":\"show tables;\",\"_token\":\"iWqZDvR2hzMmriEis9jRP1d0O59gFb2NWOTVGm56\"}','2017-06-15 10:41:17','2017-06-15 10:41:17'),(116,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-15 11:18:27','2017-06-15 11:18:27'),(117,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-15 11:18:34','2017-06-15 11:18:34'),(118,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-15 20:37:55','2017-06-15 20:37:55'),(119,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-15 20:43:23','2017-06-15 20:43:23'),(120,1,'admin','GET','192.168.10.1','[]','2017-06-21 12:50:45','2017-06-21 12:50:45'),(121,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 12:50:52','2017-06-21 12:50:52'),(122,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 12:50:57','2017-06-21 12:50:57'),(123,1,'admin/api/genders','GET','192.168.10.1','{\"q\":\"M\"}','2017-06-21 12:51:47','2017-06-21 12:51:47'),(124,1,'admin','GET','192.168.10.1','[]','2017-06-21 13:00:04','2017-06-21 13:00:04'),(125,1,'admin/candidates','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 13:00:07','2017-06-21 13:00:07'),(126,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 13:00:11','2017-06-21 13:00:11'),(127,1,'admin/candidates','GET','192.168.10.1','[]','2017-06-21 13:02:38','2017-06-21 13:02:38'),(128,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 13:02:41','2017-06-21 13:02:41'),(129,1,'admin/candidates','POST','192.168.10.1','{\"lb_gender_id\":null,\"lb_last_name\":null,\"lb_first_name\":null,\"lb_middle_name\":null,\"lb_dob\":null,\"lb_email\":null,\"lb_primary_phone\":null,\"lb_fax\":null,\"lb_facebook_url\":null,\"lb_twitter_url\":null,\"lb_linkedin_url\":null,\"lb_extra_activity\":null,\"lb_hobbies\":null,\"lb_graduate_date\":null,\"lb_start_date\":null,\"lb_end_date\":null,\"lb_company_name\":null,\"_token\":\"t2wIzGhdYWOs8v1d8fzGCVeLBJ9qBohzLQBj85nG\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/candidates\"}','2017-06-21 13:02:45','2017-06-21 13:02:45'),(130,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:02:46','2017-06-21 13:02:46'),(131,1,'admin/candidates','POST','192.168.10.1','{\"lb_gender_id\":null,\"lb_last_name\":null,\"lb_first_name\":null,\"lb_middle_name\":null,\"lb_dob\":null,\"lb_email\":null,\"lb_primary_phone\":null,\"lb_fax\":null,\"lb_facebook_url\":null,\"lb_twitter_url\":null,\"lb_linkedin_url\":null,\"lb_extra_activity\":null,\"lb_hobbies\":null,\"lb_graduate_date\":null,\"lb_start_date\":null,\"lb_end_date\":null,\"lb_company_name\":null,\"_token\":\"t2wIzGhdYWOs8v1d8fzGCVeLBJ9qBohzLQBj85nG\"}','2017-06-21 13:05:52','2017-06-21 13:05:52'),(132,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:05:53','2017-06-21 13:05:53'),(133,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:06:26','2017-06-21 13:06:26'),(134,1,'admin/candidates','GET','192.168.10.1','[]','2017-06-21 13:07:31','2017-06-21 13:07:31'),(135,1,'admin/candidates/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-21 13:07:34','2017-06-21 13:07:34'),(136,1,'admin/candidates','POST','192.168.10.1','{\"lb_gender_id\":null,\"lb_last_name\":null,\"lb_first_name\":null,\"lb_middle_name\":null,\"lb_dob\":null,\"lb_email\":null,\"lb_primary_phone\":null,\"lb_fax\":null,\"lb_facebook_url\":null,\"lb_twitter_url\":null,\"lb_linkedin_url\":null,\"lb_extra_activity\":null,\"lb_hobbies\":null,\"lb_graduate_date\":null,\"lb_start_date\":null,\"lb_end_date\":null,\"lb_company_name\":null,\"_token\":\"t2wIzGhdYWOs8v1d8fzGCVeLBJ9qBohzLQBj85nG\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/candidates\"}','2017-06-21 13:07:38','2017-06-21 13:07:38'),(137,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:07:39','2017-06-21 13:07:39'),(138,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:13:16','2017-06-21 13:13:16'),(139,1,'admin/candidates','POST','192.168.10.1','{\"lb_gender_id\":null,\"lb_last_name\":null,\"lb_first_name\":null,\"lb_middle_name\":null,\"lb_dob\":null,\"lb_email\":null,\"lb_primary_phone\":null,\"lb_fax\":null,\"lb_facebook_url\":null,\"lb_twitter_url\":null,\"lb_linkedin_url\":null,\"lb_extra_activity\":null,\"lb_hobbies\":null,\"lb_graduate_date\":null,\"lb_start_date\":null,\"lb_end_date\":null,\"lb_company_name\":null,\"_token\":\"t2wIzGhdYWOs8v1d8fzGCVeLBJ9qBohzLQBj85nG\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/candidates\"}','2017-06-21 13:13:21','2017-06-21 13:13:21'),(140,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:13:22','2017-06-21 13:13:22'),(141,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:16:02','2017-06-21 13:16:02'),(142,1,'admin/candidates/create','GET','192.168.10.1','[]','2017-06-21 13:16:47','2017-06-21 13:16:47'),(143,1,'admin','GET','192.168.10.1','[]','2017-06-26 15:20:06','2017-06-26 15:20:06'),(144,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-26 15:29:17','2017-06-26 15:29:17'),(145,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-26 15:29:22','2017-06-26 15:29:22'),(146,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-26 15:35:04','2017-06-26 15:35:04'),(147,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Job Categories\",\"icon\":\"fa-bars\",\"uri\":\"\\/categories\",\"roles\":[\"1\",null],\"_token\":\"KZUAR3FtawheAVE7jEko0sUMrVDQZnFpTjGEzOiZ\"}','2017-06-26 15:35:41','2017-06-26 15:35:41'),(148,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-26 15:35:43','2017-06-26 15:35:43'),(149,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-26 15:35:48','2017-06-26 15:35:48'),(150,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 15:36:12','2017-06-26 15:36:12'),(151,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 15:38:15','2017-06-26 15:38:15'),(152,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 17:57:42','2017-06-26 17:57:42'),(153,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 18:01:29','2017-06-26 18:01:29'),(154,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 18:04:06','2017-06-26 18:04:06'),(155,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 22:00:12','2017-06-26 22:00:12'),(156,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 22:04:18','2017-06-26 22:04:18'),(157,1,'admin/categories','GET','192.168.10.1','[]','2017-06-26 22:05:17','2017-06-26 22:05:17'),(158,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 04:54:00','2017-06-27 04:54:00'),(159,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 04:55:27','2017-06-27 04:55:27'),(160,1,'admin/categories/create','GET','192.168.10.1','[]','2017-06-27 05:01:23','2017-06-27 05:01:23'),(161,1,'admin/categories/create','GET','192.168.10.1','[]','2017-06-27 05:02:20','2017-06-27 05:02:20'),(162,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 09:41:36','2017-06-27 09:41:36'),(163,1,'admin/categories/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 09:41:41','2017-06-27 09:41:41'),(164,1,'admin/categories/create','GET','192.168.10.1','[]','2017-06-27 09:42:22','2017-06-27 09:42:22'),(165,1,'admin/categories/create','GET','192.168.10.1','[]','2017-06-27 09:43:41','2017-06-27 09:43:41'),(166,1,'admin/categories','POST','192.168.10.1','{\"lb_category_name\":\"Category 1\",\"lb_description\":\"This is a job category\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 09:49:09','2017-06-27 09:49:09'),(167,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 09:49:11','2017-06-27 09:49:11'),(168,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 09:50:34','2017-06-27 09:50:34'),(169,1,'admin/categories/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 09:50:39','2017-06-27 09:50:39'),(170,1,'admin/categories/1/edit','GET','192.168.10.1','[]','2017-06-27 09:50:40','2017-06-27 09:50:40'),(171,1,'admin/categories/1/edit','GET','192.168.10.1','[]','2017-06-27 09:52:20','2017-06-27 09:52:20'),(172,1,'admin/categories/1','PUT','192.168.10.1','{\"lb_category_name\":\"Category 1\",\"lb_description\":\"This is a job category.\\r\\nUpdate\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/categories\"}','2017-06-27 09:53:34','2017-06-27 09:53:34'),(173,1,'admin/categories','GET','192.168.10.1','[]','2017-06-27 09:53:35','2017-06-27 09:53:35'),(174,1,'admin/categories/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 09:53:41','2017-06-27 09:53:41'),(175,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:10:38','2017-06-27 11:10:38'),(176,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Degree type\",\"icon\":\"fa-bars\",\"uri\":\"\\/degrees\",\"roles\":[\"1\",null],\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:11:22','2017-06-27 11:11:22'),(177,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 11:11:24','2017-06-27 11:11:24'),(178,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:11:49','2017-06-27 11:11:49'),(179,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_degree_type\",\"model_name\":\"App\\\\Models\\\\DegreeType\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\DegreeTypeController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_degree_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_is_delete\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:13:46','2017-06-27 11:13:46'),(180,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 11:13:57','2017-06-27 11:13:57'),(181,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:15:46','2017-06-27 11:15:46'),(182,1,'admin/auth/menu','POST','192.168.10.1','{\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11}]},{\\\"id\\\":12},{\\\"id\\\":13},{\\\"id\\\":14},{\\\"id\\\":15}]\"}','2017-06-27 11:16:38','2017-06-27 11:16:38'),(183,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:16:40','2017-06-27 11:16:40'),(184,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 11:16:42','2017-06-27 11:16:42'),(185,1,'admin/degrees','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:18:11','2017-06-27 11:18:11'),(186,1,'admin/degrees/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:18:28','2017-06-27 11:18:28'),(187,1,'admin/degrees','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:18:34','2017-06-27 11:18:34'),(188,1,'admin/degrees','GET','192.168.10.1','[]','2017-06-27 11:19:44','2017-06-27 11:19:44'),(189,1,'admin/degrees/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:19:48','2017-06-27 11:19:48'),(190,1,'admin/degrees/create','GET','192.168.10.1','[]','2017-06-27 11:20:56','2017-06-27 11:20:56'),(191,1,'admin/degrees/create','GET','192.168.10.1','[]','2017-06-27 11:23:03','2017-06-27 11:23:03'),(192,1,'admin/degrees','POST','192.168.10.1','{\"lb_degree_name\":\"Associate degree\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:23:43','2017-06-27 11:23:43'),(193,1,'admin/degrees/create','GET','192.168.10.1','[]','2017-06-27 11:23:45','2017-06-27 11:23:45'),(194,1,'admin/degrees','POST','192.168.10.1','{\"lb_degree_name\":\"Associate degree\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:25:02','2017-06-27 11:25:02'),(195,1,'admin/degrees','GET','192.168.10.1','[]','2017-06-27 11:25:03','2017-06-27 11:25:03'),(196,1,'admin/degrees','GET','192.168.10.1','[]','2017-06-27 11:25:47','2017-06-27 11:25:47'),(197,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:31:06','2017-06-27 11:31:06'),(198,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Salary payment\",\"icon\":\"fa-bars\",\"uri\":\"paymentslot\",\"roles\":[\"1\",null],\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:32:21','2017-06-27 11:32:21'),(199,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 11:32:23','2017-06-27 11:32:23'),(200,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:32:31','2017-06-27 11:32:31'),(201,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_salary_payment_slot\",\"model_name\":\"App\\\\Models\\\\SalaryPayment\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SalaryPaymentController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_slot_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:33:44','2017-06-27 11:33:44'),(202,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 11:33:46','2017-06-27 11:33:46'),(203,1,'admin/degrees','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:35:34','2017-06-27 11:35:34'),(204,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:35:39','2017-06-27 11:35:39'),(205,1,'admin/auth/menu/16/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:36:09','2017-06-27 11:36:09'),(206,1,'admin/auth/menu/16/edit','GET','192.168.10.1','[]','2017-06-27 11:37:05','2017-06-27 11:37:05'),(207,1,'admin/paymentslot','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:37:09','2017-06-27 11:37:09'),(208,1,'admin/paymentslot/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:37:15','2017-06-27 11:37:15'),(209,1,'admin/paymentslot/create','GET','192.168.10.1','[]','2017-06-27 11:39:23','2017-06-27 11:39:23'),(210,1,'admin/paymentslot','POST','192.168.10.1','{\"lb_slot_name\":\"Hour\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:39:49','2017-06-27 11:39:49'),(211,1,'admin/paymentslot','GET','192.168.10.1','[]','2017-06-27 11:39:51','2017-06-27 11:39:51'),(212,1,'admin/paymentslot','GET','192.168.10.1','[]','2017-06-27 11:40:29','2017-06-27 11:40:29'),(213,1,'admin/paymentslot/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:40:33','2017-06-27 11:40:33'),(214,1,'admin/paymentslot','POST','192.168.10.1','{\"lb_slot_name\":\"Day\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/paymentslot\"}','2017-06-27 11:40:44','2017-06-27 11:40:44'),(215,1,'admin/paymentslot','GET','192.168.10.1','[]','2017-06-27 11:40:45','2017-06-27 11:40:45'),(216,1,'admin/paymentslot/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:40:49','2017-06-27 11:40:49'),(217,1,'admin/paymentslot','POST','192.168.10.1','{\"lb_slot_name\":\"Week\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/paymentslot\"}','2017-06-27 11:40:54','2017-06-27 11:40:54'),(218,1,'admin/paymentslot','GET','192.168.10.1','[]','2017-06-27 11:40:55','2017-06-27 11:40:55'),(219,1,'admin/paymentslot','GET','192.168.10.1','{\"_export_\":\"1\"}','2017-06-27 11:40:58','2017-06-27 11:40:58'),(220,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:44:02','2017-06-27 11:44:02'),(221,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Skills Period\",\"icon\":\"fa-bars\",\"uri\":\"skillsperiod\",\"roles\":[\"1\",null],\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:44:33','2017-06-27 11:44:33'),(222,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 11:44:35','2017-06-27 11:44:35'),(223,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:44:43','2017-06-27 11:44:43'),(224,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_skills_period\",\"model_name\":\"App\\\\Models\\\\SkillsPeriod\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\SkillsPeriodController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_skills_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\"}','2017-06-27 11:45:38','2017-06-27 11:45:38'),(225,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 11:45:40','2017-06-27 11:45:40'),(226,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:46:23','2017-06-27 11:46:23'),(227,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 11:46:55','2017-06-27 11:46:55'),(228,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:46:55','2017-06-27 11:46:55'),(229,1,'admin/skillsperiod','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:46:59','2017-06-27 11:46:59'),(230,1,'admin/skillsperiod','GET','192.168.10.1','[]','2017-06-27 11:48:38','2017-06-27 11:48:38'),(231,1,'admin/skillsperiod','GET','192.168.10.1','{\"_export_\":\"1\"}','2017-06-27 11:48:42','2017-06-27 11:48:42'),(232,1,'admin/skillsperiod/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 11:48:46','2017-06-27 11:48:46'),(233,1,'admin/skillsperiod','POST','192.168.10.1','{\"lb_skills_name\":\"1 - 12 months\",\"_token\":\"zcVFR4WAihw52JyTq1uXMVspuHm8tARjOUQ2Pcxn\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/skillsperiod\"}','2017-06-27 11:49:01','2017-06-27 11:49:01'),(234,1,'admin/skillsperiod','GET','192.168.10.1','[]','2017-06-27 11:49:02','2017-06-27 11:49:02'),(235,1,'admin','GET','192.168.10.1','[]','2017-06-27 15:53:43','2017-06-27 15:53:43'),(236,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 15:53:53','2017-06-27 15:53:53'),(237,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Pay Frequency\",\"icon\":\"fa-bars\",\"uri\":\"\\/payfrequency\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 15:54:24','2017-06-27 15:54:24'),(238,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 15:54:25','2017-06-27 15:54:25'),(239,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 15:54:32','2017-06-27 15:54:32'),(240,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 15:54:41','2017-06-27 15:54:41'),(241,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_pay_frequency\",\"model_name\":\"App\\\\Models\\\\PayFrequency\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\PayFrequencyController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_pay_frequency_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 15:56:44','2017-06-27 15:56:44'),(242,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 15:56:54','2017-06-27 15:56:54'),(243,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 15:57:10','2017-06-27 15:57:10'),(244,1,'admin/auth/menu/16/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 15:57:17','2017-06-27 15:57:17'),(245,1,'admin/auth/menu/16','PUT','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Pay rate Schedule\",\"icon\":\"fa-bars\",\"uri\":\"paymentslot\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/menu\"}','2017-06-27 15:57:22','2017-06-27 15:57:22'),(246,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 15:57:23','2017-06-27 15:57:23'),(247,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 15:57:39','2017-06-27 15:57:39'),(248,1,'admin/payfrequency','GET','192.168.10.1','[]','2017-06-27 15:58:53','2017-06-27 15:58:53'),(249,1,'admin/payfrequency/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 15:59:30','2017-06-27 15:59:30'),(250,1,'admin/payfrequency/create','GET','192.168.10.1','[]','2017-06-27 16:05:31','2017-06-27 16:05:31'),(251,1,'admin/payfrequency/create','GET','192.168.10.1','[]','2017-06-27 16:07:22','2017-06-27 16:07:22'),(252,1,'admin/payfrequency','POST','192.168.10.1','{\"lb_pay_frequency_name\":\"Daily\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 16:09:41','2017-06-27 16:09:41'),(253,1,'admin/payfrequency','GET','192.168.10.1','[]','2017-06-27 16:09:43','2017-06-27 16:09:43'),(254,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 16:22:00','2017-06-27 16:22:00'),(255,1,'admin/auth/menu/16/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 16:22:06','2017-06-27 16:22:06'),(256,1,'admin/auth/menu/16','PUT','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Pay Rate Schedule\",\"icon\":\"fa-bars\",\"uri\":\"paymentslot\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/auth\\/menu\"}','2017-06-27 16:22:13','2017-06-27 16:22:13'),(257,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 16:22:14','2017-06-27 16:22:14'),(258,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:12:45','2017-06-27 18:12:45'),(259,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Geo Datasource\",\"icon\":\"fa-bars\",\"uri\":\"#\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:13:46','2017-06-27 18:13:46'),(260,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 18:13:48','2017-06-27 18:13:48'),(261,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"19\",\"title\":\"Countries\",\"icon\":\"fa-bars\",\"uri\":\"\\/countries\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:14:27','2017-06-27 18:14:27'),(262,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 18:14:28','2017-06-27 18:14:28'),(263,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"19\",\"title\":\"States\",\"icon\":\"fa-bars\",\"uri\":\"\\/states\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:15:06','2017-06-27 18:15:06'),(264,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 18:15:07','2017-06-27 18:15:07'),(265,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"19\",\"title\":\"Cities\",\"icon\":\"fa-bars\",\"uri\":\"\\/cities\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:15:29','2017-06-27 18:15:29'),(266,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 18:15:30','2017-06-27 18:15:30'),(267,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 18:15:33','2017-06-27 18:15:33'),(268,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:16:46','2017-06-27 18:16:46'),(269,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_country\",\"model_name\":\"App\\\\Models\\\\TblCountry\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\CountryController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_country_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_country_code\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"lb_active\",\"type\":\"integer\",\"key\":null,\"default\":\"0\",\"comment\":\"By default any country that is added is disabled\"}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:18:42','2017-06-27 18:18:42'),(270,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 18:18:43','2017-06-27 18:18:43'),(271,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 18:22:49','2017-06-27 18:22:49'),(272,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:23:20','2017-06-27 18:23:20'),(273,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:23:48','2017-06-27 18:23:48'),(274,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:23:49','2017-06-27 18:23:49'),(275,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:24:12','2017-06-27 18:24:12'),(276,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:24:14','2017-06-27 18:24:14'),(277,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:24:15','2017-06-27 18:24:15'),(278,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:24:56','2017-06-27 18:24:56'),(279,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:25:00','2017-06-27 18:25:00'),(280,1,'admin/countries/create','GET','192.168.10.1','[]','2017-06-27 18:25:45','2017-06-27 18:25:45'),(281,1,'admin/countries','POST','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:26:47','2017-06-27 18:26:47'),(282,1,'admin/countries/create','GET','192.168.10.1','[]','2017-06-27 18:26:48','2017-06-27 18:26:48'),(283,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:28:31','2017-06-27 18:28:31'),(284,1,'admin/helpers/terminal/artisan','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:33:09','2017-06-27 18:33:09'),(285,1,'admin/helpers/terminal/artisan','POST','192.168.10.1','{\"c\":\"php artisan migrate --force\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:33:21','2017-06-27 18:33:21'),(286,1,'admin/helpers/terminal/artisan','POST','192.168.10.1','{\"c\":\"migrate --force\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:33:33','2017-06-27 18:33:33'),(287,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:33:58','2017-06-27 18:33:58'),(288,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_country\",\"model_name\":\"App\\\\Models\\\\TblCountry\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\CountryController\",\"create\":[\"migrate\"],\"fields\":[{\"name\":null,\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:34:39','2017-06-27 18:34:39'),(289,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 18:34:40','2017-06-27 18:34:40'),(290,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_country\",\"model_name\":\"App\\\\Models\\\\TblCountry\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\CountryController\",\"create\":[\"migration\",\"migrate\"],\"fields\":[{\"name\":\"lb_country_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_country_code\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"lb_active\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":\"0\",\"comment\":\"By default any country that is added is disabled\"}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 18:40:12','2017-06-27 18:40:12'),(291,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 18:40:14','2017-06-27 18:40:14'),(292,1,'admin/countries','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:42:09','2017-06-27 18:42:09'),(293,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:42:12','2017-06-27 18:42:12'),(294,1,'admin/countries','POST','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:42:25','2017-06-27 18:42:25'),(295,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:42:26','2017-06-27 18:42:26'),(296,1,'admin/countries/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:42:42','2017-06-27 18:42:42'),(297,1,'admin/countries/1/edit','GET','192.168.10.1','[]','2017-06-27 18:47:42','2017-06-27 18:47:42'),(298,1,'admin/countries/1','PUT','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\"}','2017-06-27 18:48:21','2017-06-27 18:48:21'),(299,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:48:22','2017-06-27 18:48:22'),(300,1,'admin/countries/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:48:26','2017-06-27 18:48:26'),(301,1,'admin/countries/1','PUT','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:49:25','2017-06-27 18:49:25'),(302,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:49:26','2017-06-27 18:49:26'),(303,1,'admin/countries/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:49:30','2017-06-27 18:49:30'),(304,1,'admin/countries/1','PUT','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US3\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:50:05','2017-06-27 18:50:05'),(305,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:50:06','2017-06-27 18:50:06'),(306,1,'admin/countries/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:50:11','2017-06-27 18:50:11'),(307,1,'admin/countries/1','PUT','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"lb_active\":\"0\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:50:51','2017-06-27 18:50:51'),(308,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:50:52','2017-06-27 18:50:52'),(309,1,'admin/countries/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:50:56','2017-06-27 18:50:56'),(310,1,'admin/countries/1','PUT','192.168.10.1','{\"lb_country_name\":\"United State\",\"lb_country_code\":\"US\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:53:43','2017-06-27 18:53:43'),(311,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:53:45','2017-06-27 18:53:45'),(312,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:53:49','2017-06-27 18:53:49'),(313,1,'admin/countries','POST','192.168.10.1','{\"lb_country_name\":\"Cameroon\",\"lb_country_code\":\"CM\",\"lb_active\":null,\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:53:59','2017-06-27 18:53:59'),(314,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:54:00','2017-06-27 18:54:00'),(315,1,'admin/countries/2/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:54:04','2017-06-27 18:54:04'),(316,1,'admin/countries/2','PUT','192.168.10.1','{\"lb_country_name\":\"Cameroon\",\"lb_country_code\":\"CM\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:54:09','2017-06-27 18:54:09'),(317,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:54:10','2017-06-27 18:54:10'),(318,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:54:28','2017-06-27 18:54:28'),(319,1,'admin/countries','POST','192.168.10.1','{\"lb_country_name\":\"France\",\"lb_country_code\":\"FR\",\"lb_active\":null,\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:54:37','2017-06-27 18:54:37'),(320,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:54:38','2017-06-27 18:54:38'),(321,1,'admin/countries/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:55:11','2017-06-27 18:55:11'),(322,1,'admin/countries','POST','192.168.10.1','{\"lb_country_name\":\"Canada\",\"lb_country_code\":\"CN\",\"lb_active\":\"0\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:55:21','2017-06-27 18:55:21'),(323,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:55:21','2017-06-27 18:55:21'),(324,1,'admin/countries/4/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:55:33','2017-06-27 18:55:33'),(325,1,'admin/countries/4','PUT','192.168.10.1','{\"lb_country_name\":\"Canada\",\"lb_country_code\":\"CN\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 18:55:38','2017-06-27 18:55:38'),(326,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 18:55:39','2017-06-27 18:55:39'),(327,1,'admin/countries/4/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:55:43','2017-06-27 18:55:43'),(328,1,'admin/countries','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:56:02','2017-06-27 18:56:02'),(329,1,'admin/countries','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 18:57:37','2017-06-27 18:57:37'),(330,1,'admin/countries','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:01:56','2017-06-27 19:01:56'),(331,1,'admin/countries/4/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:02:11','2017-06-27 19:02:11'),(332,1,'admin/countries/4','PUT','192.168.10.1','{\"lb_country_name\":\"Canada\",\"lb_country_code\":\"CA\",\"lb_active\":\"1\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/countries\"}','2017-06-27 19:02:19','2017-06-27 19:02:19'),(333,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 19:02:20','2017-06-27 19:02:20'),(334,1,'admin/countries','GET','192.168.10.1','[]','2017-06-27 19:02:37','2017-06-27 19:02:37'),(335,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:05:49','2017-06-27 19:05:49'),(336,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_states\",\"model_name\":\"App\\\\Models\\\\TblStates\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\StateController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_state_name\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"lb_state_code\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"lb_country_code\",\"type\":\"string\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 19:07:27','2017-06-27 19:07:27'),(337,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 19:07:29','2017-06-27 19:07:29'),(338,1,'admin','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:08:23','2017-06-27 19:08:23'),(339,1,'admin/states','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:08:27','2017-06-27 19:08:27'),(340,1,'admin/states','GET','192.168.10.1','[]','2017-06-27 19:15:08','2017-06-27 19:15:08'),(341,1,'admin/states/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 19:52:27','2017-06-27 19:52:27'),(342,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:04:19','2017-06-27 20:04:19'),(343,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:05:17','2017-06-27 20:05:17'),(344,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:06:20','2017-06-27 20:06:20'),(345,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:07:27','2017-06-27 20:07:27'),(346,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 20:07:33','2017-06-27 20:07:33'),(347,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 20:07:33','2017-06-27 20:07:33'),(348,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Cam\"}','2017-06-27 20:08:26','2017-06-27 20:08:26'),(349,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:08:38','2017-06-27 20:08:38'),(350,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 20:08:42','2017-06-27 20:08:42'),(351,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 20:08:52','2017-06-27 20:08:52'),(352,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 20:08:54','2017-06-27 20:08:54'),(353,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Camer\"}','2017-06-27 20:08:55','2017-06-27 20:08:55'),(354,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 20:09:15','2017-06-27 20:09:15'),(355,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 20:09:17','2017-06-27 20:09:17'),(356,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 20:09:56','2017-06-27 20:09:56'),(357,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 20:09:57','2017-06-27 20:09:57'),(358,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:57:49','2017-06-27 20:57:49'),(359,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 20:58:39','2017-06-27 20:58:39'),(360,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 21:00:44','2017-06-27 21:00:44'),(361,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 21:04:48','2017-06-27 21:04:48'),(362,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"G\"}','2017-06-27 21:05:19','2017-06-27 21:05:19'),(363,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Fr\"}','2017-06-27 21:05:21','2017-06-27 21:05:21'),(364,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 22:25:16','2017-06-27 22:25:16'),(365,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 22:36:36','2017-06-27 22:36:36'),(366,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 22:37:33','2017-06-27 22:37:33'),(367,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 22:42:22','2017-06-27 22:42:22'),(368,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 22:55:47','2017-06-27 22:55:47'),(369,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:07:22','2017-06-27 23:07:22'),(370,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 23:07:28','2017-06-27 23:07:28'),(371,1,'admin/states','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:10:23','2017-06-27 23:10:23'),(372,1,'admin/states/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:10:46','2017-06-27 23:10:46'),(373,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:12:00','2017-06-27 23:12:00'),(374,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:13:03','2017-06-27 23:13:03'),(375,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Camer\"}','2017-06-27 23:13:08','2017-06-27 23:13:08'),(376,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 23:13:08','2017-06-27 23:13:08'),(377,1,'admin/states','POST','192.168.10.1','{\"lb_country_code\":\"2\",\"lb_state_name\":\"lwkjvkjjneke\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:13:13','2017-06-27 23:13:13'),(378,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:13:14','2017-06-27 23:13:14'),(379,1,'admin/states','POST','192.168.10.1','{\"lb_country_code\":null,\"lb_state_name\":\"lwkjvkjjneke\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:13:36','2017-06-27 23:13:36'),(380,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:13:37','2017-06-27 23:13:37'),(381,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:14:14','2017-06-27 23:14:14'),(382,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Ca\"}','2017-06-27 23:14:20','2017-06-27 23:14:20'),(383,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Cam\"}','2017-06-27 23:14:20','2017-06-27 23:14:20'),(384,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 23:14:21','2017-06-27 23:14:21'),(385,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Came\"}','2017-06-27 23:14:22','2017-06-27 23:14:22'),(386,1,'admin/states','POST','192.168.10.1','{\"lb_country_code\":\"2\",\"lb_state_name\":\"klsjndfskjjksfj\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:14:27','2017-06-27 23:14:27'),(387,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:14:28','2017-06-27 23:14:28'),(388,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:16:58','2017-06-27 23:16:58'),(389,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"C\"}','2017-06-27 23:17:03','2017-06-27 23:17:03'),(390,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Cam\"}','2017-06-27 23:17:04','2017-06-27 23:17:04'),(391,1,'admin/states','POST','192.168.10.1','{\"lb_country_code\":\"2\",\"lb_state_name\":\"f\\/;dfslkdfklfd\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:17:08','2017-06-27 23:17:08'),(392,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:17:09','2017-06-27 23:17:09'),(393,1,'admin/api/country/list','GET','192.168.10.1','{\"q\":\"Fr\"}','2017-06-27 23:19:50','2017-06-27 23:19:50'),(394,1,'admin/states','POST','192.168.10.1','{\"lb_country_code\":\"3\",\"lb_state_name\":\"f\\/;dfslkdfklfd\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:19:54','2017-06-27 23:19:54'),(395,1,'admin/states/create','GET','192.168.10.1','[]','2017-06-27 23:19:56','2017-06-27 23:19:56'),(396,1,'admin/countries','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:20:31','2017-06-27 23:20:31'),(397,1,'admin/countries/4/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:20:49','2017-06-27 23:20:49'),(398,1,'admin/payfrequency','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:21:52','2017-06-27 23:21:52'),(399,1,'admin/categories','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:29:13','2017-06-27 23:29:13'),(400,1,'admin/helpers/scaffold','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:29:45','2017-06-27 23:29:45'),(401,1,'admin/helpers/scaffold','POST','192.168.10.1','{\"table_name\":\"tbl_job_type\",\"model_name\":\"App\\\\Models\\\\JobType\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\JobTypeController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"lb_job_type_name\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:31:25','2017-06-27 23:31:25'),(402,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 23:31:28','2017-06-27 23:31:28'),(403,1,'admin/helpers/scaffold','GET','192.168.10.1','[]','2017-06-27 23:34:50','2017-06-27 23:34:50'),(404,1,'admin/auth/menu','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:35:00','2017-06-27 23:35:00'),(405,1,'admin/auth/menu','POST','192.168.10.1','{\"parent_id\":\"0\",\"title\":\"Job Type\",\"icon\":\"fa-bars\",\"uri\":\"\\/jobtypes\",\"roles\":[\"1\",null],\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-27 23:35:32','2017-06-27 23:35:32'),(406,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 23:35:33','2017-06-27 23:35:33'),(407,1,'admin/auth/menu','GET','192.168.10.1','[]','2017-06-27 23:35:36','2017-06-27 23:35:36'),(408,1,'admin/jobtypes','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:35:40','2017-06-27 23:35:40'),(409,1,'admin/jobtypes/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-27 23:35:46','2017-06-27 23:35:46'),(410,1,'admin/jobtypes','POST','192.168.10.1','{\"lb_job_type_name\":\"Full Time\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/jobtypes\"}','2017-06-27 23:35:59','2017-06-27 23:35:59'),(411,1,'admin/jobtypes','GET','192.168.10.1','[]','2017-06-27 23:35:59','2017-06-27 23:35:59'),(412,1,'admin/category/list','GET','192.168.10.1','[]','2017-06-28 00:10:11','2017-06-28 00:10:11'),(413,1,'admin/auth/logout','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-28 00:11:09','2017-06-28 00:11:09'),(414,1,'admin/category/list','GET','192.168.10.1','[]','2017-06-28 00:11:35','2017-06-28 00:11:35'),(415,1,'admin/auth/login','POST','192.168.10.1','{\"username\":\"admin\",\"password\":\"admin\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\"}','2017-06-28 00:37:15','2017-06-28 00:37:15'),(416,1,'admin','GET','192.168.10.1','[]','2017-06-28 00:37:17','2017-06-28 00:37:17'),(417,1,'admin/skillsperiod','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-28 00:37:23','2017-06-28 00:37:23'),(418,1,'admin/skillsperiod/1/edit','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-28 00:46:58','2017-06-28 00:46:58'),(419,1,'admin/skillsperiod/1','PUT','192.168.10.1','{\"lb_skills_name\":\"Months\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/skillsperiod\"}','2017-06-28 00:47:05','2017-06-28 00:47:05'),(420,1,'admin/skillsperiod','GET','192.168.10.1','[]','2017-06-28 00:47:06','2017-06-28 00:47:06'),(421,1,'admin/skillsperiod/create','GET','192.168.10.1','{\"_pjax\":\"#pjax-container\"}','2017-06-28 00:47:10','2017-06-28 00:47:10'),(422,1,'admin/skillsperiod','POST','192.168.10.1','{\"lb_skills_name\":\"Years\",\"_token\":\"j0iKwQ7eGHzp5e4K14055Jc0Xdh03inVtazLLJOg\",\"_previous_\":\"http:\\/\\/njl.kimberts.com\\/admin\\/skillsperiod\"}','2017-06-28 00:47:17','2017-06-28 00:47:17'),(423,1,'admin/skillsperiod','GET','192.168.10.1','[]','2017-06-28 00:47:17','2017-06-28 00:47:17');
/*!40000 ALTER TABLE `admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_permissions`
--

LOCK TABLES `admin_permissions` WRITE;
/*!40000 ALTER TABLE `admin_permissions` DISABLE KEYS */;
INSERT INTO `admin_permissions` VALUES (1,'CANDIDATE PERMISSION','candidate_permission','2017-06-12 04:33:38','2017-06-12 04:33:38'),(2,'RECRUITER PERMISSION','recruiter_permission','2017-06-12 04:34:14','2017-06-12 04:34:14'),(3,'EMPLOYER PERMISSION','employer_permission','2017-06-12 04:34:47','2017-06-12 04:34:47');
/*!40000 ALTER TABLE `admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_menu`
--

LOCK TABLES `admin_role_menu` WRITE;
/*!40000 ALTER TABLE `admin_role_menu` DISABLE KEYS */;
INSERT INTO `admin_role_menu` VALUES (1,2,NULL,NULL),(1,8,NULL,NULL),(3,12,NULL,NULL),(4,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_permissions`
--

LOCK TABLES `admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `admin_role_permissions` DISABLE KEYS */;
INSERT INTO `admin_role_permissions` VALUES (2,1,NULL,NULL),(3,2,NULL,NULL),(4,3,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_role_users`
--

LOCK TABLES `admin_role_users` WRITE;
/*!40000 ALTER TABLE `admin_role_users` DISABLE KEYS */;
INSERT INTO `admin_role_users` VALUES (1,1,NULL,NULL),(2,2,NULL,NULL);
/*!40000 ALTER TABLE `admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_roles`
--

LOCK TABLES `admin_roles` WRITE;
/*!40000 ALTER TABLE `admin_roles` DISABLE KEYS */;
INSERT INTO `admin_roles` VALUES (1,'Administrator','administrator','2017-06-12 04:27:16','2017-06-12 04:27:16'),(2,'CANDIDATE','candidate','2017-06-12 04:35:39','2017-06-12 04:35:39'),(3,'RECRUITER','recruiter','2017-06-12 04:36:06','2017-06-12 04:36:06'),(4,'EMPLOYER','employer','2017-06-12 04:36:24','2017-06-12 04:36:24');
/*!40000 ALTER TABLE `admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user_permissions`
--

LOCK TABLES `admin_user_permissions` WRITE;
/*!40000 ALTER TABLE `admin_user_permissions` DISABLE KEYS */;
INSERT INTO `admin_user_permissions` VALUES (2,1,NULL,NULL);
/*!40000 ALTER TABLE `admin_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','$2y$10$1DibyTerSo5aL8q3Y5WENOMM7FQrbeZINh9delIbxBdiNQXCFePfy','Kwaye Kant','image/IMG_20170116_164506[1].jpg','RYCp4th81LxETO4wHpOmbegbFO2htFVC8TBTcrBg7NxfgkpWsfaBdxm0mNnt','2017-06-12 04:27:16','2017-06-12 04:55:37'),(2,'gabykant','$2y$10$DdKrOBh0h6A/EXtso9mMMOut5ycPKI3LVNXUiZaFgP6JWHWJh.jsO','Kwaye Kant',NULL,NULL,'2017-06-12 04:38:58','2017-06-12 04:38:58');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2016_01_04_173148_create_admin_tables',1),(2,'2014_10_12_000000_create_users_table',2),(3,'2014_10_12_100000_create_password_resets_table',2),(4,'2017_06_15_081853_create_tbl_program_studying_table',3),(5,'2017_06_27_111347_create_tbl_degree_type_table',4),(6,'2017_06_27_113344_create_tbl_salary_payment_slot_table',5),(7,'2017_06_27_114538_create_tbl_skills_period_table',6),(8,'2017_06_27_155644_create_tbl_pay_frequency_table',7),(9,'2017_06_27_181842_create_tbl_country_table',8),(10,'2017_06_27_184013_create_tbl_country_table',9),(11,'2017_06_27_190728_create_tbl_states_table',10),(12,'2017_06_27_233125_create_tbl_job_type_table',11);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_award`
--

DROP TABLE IF EXISTS `tbl_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_award` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_issuing_organization` varchar(45) DEFAULT NULL,
  `lb_date` int(11) DEFAULT NULL,
  `lb_place` varchar(45) DEFAULT NULL,
  `lb_description` text,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_award_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_award`
--

LOCK TABLES `tbl_award` WRITE;
/*!40000 ALTER TABLE `tbl_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_candidate`
--

DROP TABLE IF EXISTS `tbl_candidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_user_id` int(11) DEFAULT NULL,
  `lb_gender_id` int(11) DEFAULT NULL COMMENT 'This is the foreign of the Gender table',
  `lb_last_name` varchar(255) DEFAULT NULL,
  `lb_first_name` varchar(255) DEFAULT NULL,
  `lb_middle_name` varchar(255) DEFAULT NULL,
  `lb_dob` int(11) DEFAULT NULL COMMENT '''The day of the birth. Stored in the INT format''',
  `lb_email` varchar(255) DEFAULT NULL,
  `lb_primary_phone` varchar(255) DEFAULT NULL,
  `lb_fax` varchar(255) DEFAULT NULL,
  `lb_facebook_url` varchar(255) DEFAULT NULL,
  `lb_twitter_url` varchar(255) DEFAULT NULL,
  `lb_linkedin_url` varchar(255) DEFAULT NULL,
  `lb_extra_activity` text,
  `lb_hobbies` text,
  `lb_photo_url` varchar(255) DEFAULT NULL,
  `lb_photo_full_portrait` varchar(255) DEFAULT NULL,
  `lb_graduate_date` int(11) DEFAULT NULL,
  `lb_program_studying_id` int(11) DEFAULT NULL,
  `lb_degree_type_id` int(11) DEFAULT NULL,
  `lb_start_date` int(11) DEFAULT NULL,
  `lb_end_date` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_position` varchar(255) DEFAULT NULL,
  `lb_department` varchar(255) DEFAULT NULL,
  `lb_company_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_candidate_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_candidate`
--

LOCK TABLES `tbl_candidate` WRITE;
/*!40000 ALTER TABLE `tbl_candidate` DISABLE KEYS */;
INSERT INTO `tbl_candidate` VALUES (2,3,NULL,'Loic','kima','Gabriel',NULL,'bkima@kimberts.net',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,4,NULL,'Nkengne','Yannick','Gabriel',NULL,'yanstv@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,7,NULL,'Kwaye','Gabriel','KWAYE',NULL,'gabykant@yahoo.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,9,NULL,'Net','Actu',NULL,NULL,'g.kwaye3@ksoft-solutions.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,10,NULL,'Killer','Sis','Fdh',NULL,'sis.killer@rocketmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_candidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_candidate_skill`
--

DROP TABLE IF EXISTS `tbl_candidate_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_candidate_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_candidate_id` int(11) DEFAULT NULL COMMENT 'The foreign key of Tbl_candidate',
  `lb_skill_id` int(11) DEFAULT NULL COMMENT 'The foreign key of Tbl_skill',
  `lb_length_experience` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_candidate_skill`
--

LOCK TABLES `tbl_candidate_skill` WRITE;
/*!40000 ALTER TABLE `tbl_candidate_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_candidate_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_contact_information`
--

DROP TABLE IF EXISTS `tbl_contact_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contact_information` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_address_type` int(11) DEFAULT NULL COMMENT 'This can be 2 possible value : HomeAddress(0) or WorkAddress(1)',
  `lb_house_number` varchar(45) DEFAULT NULL,
  `lb_appartment_number` varchar(45) DEFAULT NULL,
  `lb_street_name` varchar(255) DEFAULT NULL,
  `lb_city` int(11) DEFAULT NULL,
  `lb_county` int(11) DEFAULT NULL,
  `lb_state` int(11) DEFAULT NULL,
  `lb_country` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contact_information`
--

LOCK TABLES `tbl_contact_information` WRITE;
/*!40000 ALTER TABLE `tbl_contact_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_contact_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_active` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_country_lb_country_code_unique` (`lb_country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country`
--

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;
INSERT INTO `tbl_country` VALUES (1,'United State','US',1,'2017-06-27 18:42:25','2017-06-27 18:53:43',NULL),(2,'Cameroon','CM',1,'2017-06-27 18:53:59','2017-06-27 18:54:09',NULL),(3,'France','FR',NULL,'2017-06-27 18:54:37','2017-06-27 18:54:37',NULL),(4,'Canada','CA',1,'2017-06-27 18:55:21','2017-06-27 19:02:19',NULL);
/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_day`
--

DROP TABLE IF EXISTS `tbl_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_day` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_day`
--

LOCK TABLES `tbl_day` WRITE;
/*!40000 ALTER TABLE `tbl_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_degree_type`
--

DROP TABLE IF EXISTS `tbl_degree_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_degree_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_degree_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_degree_type`
--

LOCK TABLES `tbl_degree_type` WRITE;
/*!40000 ALTER TABLE `tbl_degree_type` DISABLE KEYS */;
INSERT INTO `tbl_degree_type` VALUES (1,'Associate degree',1,'2017-06-27 11:25:02','2017-06-27 11:25:02',NULL);
/*!40000 ALTER TABLE `tbl_degree_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_education_history`
--

DROP TABLE IF EXISTS `tbl_education_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_education_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_address` varchar(45) DEFAULT NULL,
  `lb_attended_from` datetime DEFAULT NULL,
  `lb_attended_to` datetime DEFAULT NULL,
  `lb_is_graduated` set('0','1') DEFAULT '1' COMMENT 'To check if the concerned is graduated (0) or not (1)',
  `lb_education_type` varchar(45) DEFAULT NULL COMMENT 'A dropdown list to select from: HighSchool-College-University',
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_education_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_education_history`
--

LOCK TABLES `tbl_education_history` WRITE;
/*!40000 ALTER TABLE `tbl_education_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_education_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_employer`
--

DROP TABLE IF EXISTS `tbl_employer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_user_id` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_city_id` int(11) DEFAULT NULL,
  `lb_state_id` int(11) DEFAULT NULL,
  `lb_email` varchar(255) DEFAULT NULL,
  `lb_phone_number` varchar(255) DEFAULT NULL,
  `lb_fax` varchar(255) DEFAULT NULL,
  `lb_facebook_url` varchar(255) DEFAULT NULL,
  `lb_twitter_url` varchar(255) DEFAULT NULL,
  `lb_linkedin_url` varchar(255) DEFAULT NULL,
  `lb_company_news` text,
  `lb_company_logo_path` varchar(255) DEFAULT NULL,
  `lb_industry_type_id` int(11) DEFAULT NULL,
  `lb_company_portrait` text,
  `lb_employee_firstname` varchar(255) DEFAULT NULL,
  `lb_employee_middlename` varchar(255) DEFAULT NULL,
  `lb_employee_lastname` varchar(255) DEFAULT NULL,
  `lb_company_create_date` int(11) DEFAULT NULL,
  `lb_hiring_status` int(11) DEFAULT '0' COMMENT '0 for status NOWHIRING and 1 for NoVACANCY',
  `lb_position` varchar(255) DEFAULT NULL,
  `lb_department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_employer_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_employer`
--

LOCK TABLES `tbl_employer` WRITE;
/*!40000 ALTER TABLE `tbl_employer` DISABLE KEYS */;
INSERT INTO `tbl_employer` VALUES (1,14,NULL,NULL,NULL,'gabykantdd@yahoo.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Gabriel','KWAYE','Kwaye',NULL,0,NULL,NULL),(2,17,NULL,NULL,NULL,'gabrielkwaye12@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SEUNKAM','KWAYE','Loic',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `tbl_employer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_employment_status`
--

DROP TABLE IF EXISTS `tbl_employment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_employment_status` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_group_status` varchar(45) DEFAULT NULL COMMENT 'This is a dropdown list with values CURRENTLY EMPLOYED- SCHOOLING - EMPLOYED_OR_SCHOOLING',
  `lb_address` text,
  `lb_started_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_employment_status_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_employment_status`
--

LOCK TABLES `tbl_employment_status` WRITE;
/*!40000 ALTER TABLE `tbl_employment_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_employment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_extra_activity`
--

DROP TABLE IF EXISTS `tbl_extra_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_extra_activity` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_extra_activity`
--

LOCK TABLES `tbl_extra_activity` WRITE;
/*!40000 ALTER TABLE `tbl_extra_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_extra_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_gender`
--

DROP TABLE IF EXISTS `tbl_gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_gender` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_gender`
--

LOCK TABLES `tbl_gender` WRITE;
/*!40000 ALTER TABLE `tbl_gender` DISABLE KEYS */;
INSERT INTO `tbl_gender` VALUES (1,'Male',0),(2,'Female',0);
/*!40000 ALTER TABLE `tbl_gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_hiring_status`
--

DROP TABLE IF EXISTS `tbl_hiring_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_hiring_status` (
  `id` int(11) NOT NULL,
  `lb_employer_id` int(11) DEFAULT NULL,
  `lb_job_category` varchar(45) DEFAULT NULL,
  `lb_position` varchar(45) DEFAULT NULL,
  `lb_department` varchar(45) DEFAULT NULL,
  `lb_location_city` varchar(45) DEFAULT NULL,
  `lb_location_state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_hiring_status`
--

LOCK TABLES `tbl_hiring_status` WRITE;
/*!40000 ALTER TABLE `tbl_hiring_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_hiring_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_industry_type`
--

DROP TABLE IF EXISTS `tbl_industry_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_industry_type` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(255) DEFAULT NULL,
  `lb_is_delete` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_industry_type_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_industry_type`
--

LOCK TABLES `tbl_industry_type` WRITE;
/*!40000 ALTER TABLE `tbl_industry_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_industry_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job`
--

DROP TABLE IF EXISTS `tbl_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job` (
  `id` int(11) NOT NULL,
  `lb_skill_id` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_additional_skill_id` int(11) DEFAULT NULL,
  `lb_job_category_id` int(11) DEFAULT NULL,
  `lb_title` varchar(45) DEFAULT NULL,
  `lb_education` varchar(45) DEFAULT NULL,
  `lb_job_description` text,
  `lb_recruiter_id` int(11) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_job_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job`
--

LOCK TABLES `tbl_job` WRITE;
/*!40000 ALTER TABLE `tbl_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_apply`
--

DROP TABLE IF EXISTS `tbl_job_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_apply` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_job_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_job_apply_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_apply`
--

LOCK TABLES `tbl_job_apply` WRITE;
/*!40000 ALTER TABLE `tbl_job_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_job_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_category`
--

DROP TABLE IF EXISTS `tbl_job_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_category_name` varchar(255) DEFAULT NULL,
  `lb_description` text,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_category`
--

LOCK TABLES `tbl_job_category` WRITE;
/*!40000 ALTER TABLE `tbl_job_category` DISABLE KEYS */;
INSERT INTO `tbl_job_category` VALUES (1,'Category 1','This is a job category.\r\nUpdate',1);
/*!40000 ALTER TABLE `tbl_job_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_job_type`
--

DROP TABLE IF EXISTS `tbl_job_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_job_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_job_type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_job_type_lb_job_type_name_unique` (`lb_job_type_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_job_type`
--

LOCK TABLES `tbl_job_type` WRITE;
/*!40000 ALTER TABLE `tbl_job_type` DISABLE KEYS */;
INSERT INTO `tbl_job_type` VALUES (1,'Full Time','2017-06-27 23:35:59','2017-06-27 23:35:59',NULL);
/*!40000 ALTER TABLE `tbl_job_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_month`
--

DROP TABLE IF EXISTS `tbl_month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_month` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_month`
--

LOCK TABLES `tbl_month` WRITE;
/*!40000 ALTER TABLE `tbl_month` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_month` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pay_frequency`
--

DROP TABLE IF EXISTS `tbl_pay_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pay_frequency` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_pay_frequency_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pay_frequency`
--

LOCK TABLES `tbl_pay_frequency` WRITE;
/*!40000 ALTER TABLE `tbl_pay_frequency` DISABLE KEYS */;
INSERT INTO `tbl_pay_frequency` VALUES (1,'Daily','2017-06-27 16:09:42','2017-06-27 16:09:42',NULL);
/*!40000 ALTER TABLE `tbl_pay_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_professional_organization`
--

DROP TABLE IF EXISTS `tbl_professional_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_professional_organization` (
  `id` int(11) NOT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_name` varchar(255) DEFAULT NULL,
  `lb_location` text,
  `lb_participation_date` int(11) DEFAULT NULL,
  `lb_role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_professional_organization`
--

LOCK TABLES `tbl_professional_organization` WRITE;
/*!40000 ALTER TABLE `tbl_professional_organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_professional_organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_professional_training`
--

DROP TABLE IF EXISTS `tbl_professional_training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_professional_training` (
  `id` int(11) NOT NULL,
  `lb_trainer` varchar(255) DEFAULT NULL,
  `lb_training_period_from` int(11) DEFAULT NULL,
  `lb_training_period_to` int(11) DEFAULT NULL,
  `lb_certificate` varchar(45) DEFAULT NULL,
  `lb_certificate_issuing_autority` varchar(45) DEFAULT NULL,
  `lb_certificate_issued_date` int(11) DEFAULT NULL,
  `lb_certificate_expiration_date` int(11) DEFAULT NULL,
  `lb_candidate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_profesional_training_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_professional_training`
--

LOCK TABLES `tbl_professional_training` WRITE;
/*!40000 ALTER TABLE `tbl_professional_training` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_professional_training` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_program_studying`
--

DROP TABLE IF EXISTS `tbl_program_studying`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_program_studying` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_is_delete` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_program_studying`
--

LOCK TABLES `tbl_program_studying` WRITE;
/*!40000 ALTER TABLE `tbl_program_studying` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_program_studying` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_recruiter`
--

DROP TABLE IF EXISTS `tbl_recruiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_recruiter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_user_id` int(11) DEFAULT NULL,
  `lb_job_type_id` int(11) DEFAULT NULL,
  `lb_city_id` int(11) DEFAULT NULL,
  `lb_state_id` int(11) DEFAULT NULL,
  `lb_email` varchar(45) DEFAULT NULL,
  `lb_phone_number` varchar(45) DEFAULT NULL,
  `lb_fax` varchar(45) DEFAULT NULL,
  `lb_facebook_url` varchar(45) DEFAULT NULL,
  `lb_twitter_url` varchar(45) DEFAULT NULL,
  `lb_linkedin_url` varchar(45) DEFAULT NULL,
  `lb_company_news` text,
  `lb_company_logo_path` varchar(45) DEFAULT NULL,
  `lb_industry_type_id` int(11) DEFAULT NULL,
  `lb_company_portrait` text,
  `lb_hiring_firstname` varchar(255) DEFAULT NULL,
  `lb_hiring_middlename` varchar(255) DEFAULT NULL,
  `lb_hiring_lastname` varchar(255) DEFAULT NULL,
  `lb_company_create_date` int(11) DEFAULT NULL,
  `lb_hiring_status` int(11) DEFAULT '0' COMMENT '0 for status NOWHIRING and 1 for NoVACANCY',
  `lb_position` varchar(45) DEFAULT NULL,
  `lb_department` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_employer_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_recruiter`
--

LOCK TABLES `tbl_recruiter` WRITE;
/*!40000 ALTER TABLE `tbl_recruiter` DISABLE KEYS */;
INSERT INTO `tbl_recruiter` VALUES (1,16,NULL,NULL,NULL,'bkima36@kimberts.net',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'kima',NULL,'Kwaye',NULL,0,NULL,NULL),(2,18,NULL,NULL,NULL,'g.kwaye14@yahoo.de',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Gabriel Kant',NULL,'NGUEMELIEU',NULL,0,NULL,NULL),(3,19,NULL,NULL,NULL,'billing@ksoft-solutions.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Karl',NULL,'Eton',NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `tbl_recruiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reference`
--

DROP TABLE IF EXISTS `tbl_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reference` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_business_address` text,
  `lb_home_address` text,
  `lb_phone_number` varchar(45) DEFAULT NULL,
  `lb_fax` varchar(45) DEFAULT NULL,
  `lb_email` varchar(105) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_reference_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reference`
--

LOCK TABLES `tbl_reference` WRITE;
/*!40000 ALTER TABLE `tbl_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_resume`
--

DROP TABLE IF EXISTS `tbl_resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_candidate_id` int(11) DEFAULT NULL,
  `lb_resume_title` varchar(255) DEFAULT NULL,
  `lb_resume_url` varchar(255) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1' COMMENT 'If value is 1 then the row is delete and 0 if no',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_resume`
--

LOCK TABLES `tbl_resume` WRITE;
/*!40000 ALTER TABLE `tbl_resume` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_salary_payment_slot`
--

DROP TABLE IF EXISTS `tbl_salary_payment_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_salary_payment_slot` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_slot_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_salary_payment_slot`
--

LOCK TABLES `tbl_salary_payment_slot` WRITE;
/*!40000 ALTER TABLE `tbl_salary_payment_slot` DISABLE KEYS */;
INSERT INTO `tbl_salary_payment_slot` VALUES (1,'Hour','2017-06-27 11:39:49','2017-06-27 11:39:49',NULL),(2,'Day','2017-06-27 11:40:45','2017-06-27 11:40:45',NULL),(3,'Week','2017-06-27 11:40:55','2017-06-27 11:40:55',NULL);
/*!40000 ALTER TABLE `tbl_salary_payment_slot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_skill`
--

DROP TABLE IF EXISTS `tbl_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_skill` (
  `id` int(11) NOT NULL,
  `lb_name` varchar(45) DEFAULT NULL,
  `lb_is_delete` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_skill_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_skill`
--

LOCK TABLES `tbl_skill` WRITE;
/*!40000 ALTER TABLE `tbl_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_skills_period`
--

DROP TABLE IF EXISTS `tbl_skills_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_skills_period` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_skills_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_skills_period`
--

LOCK TABLES `tbl_skills_period` WRITE;
/*!40000 ALTER TABLE `tbl_skills_period` DISABLE KEYS */;
INSERT INTO `tbl_skills_period` VALUES (1,'Months','2017-06-27 11:49:01','2017-06-28 00:47:06',NULL),(2,'Years','2017-06-28 00:47:17','2017-06-28 00:47:17',NULL);
/*!40000 ALTER TABLE `tbl_skills_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_states`
--

DROP TABLE IF EXISTS `tbl_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_state_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_state_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_states_lb_state_code_unique` (`lb_state_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_states`
--

LOCK TABLES `tbl_states` WRITE;
/*!40000 ALTER TABLE `tbl_states` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_working_history`
--

DROP TABLE IF EXISTS `tbl_working_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_working_history` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lb_id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_working_history`
--

LOCK TABLES `tbl_working_history` WRITE;
/*!40000 ALTER TABLE `tbl_working_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_working_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_year`
--

DROP TABLE IF EXISTS `tbl_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lb_name_year` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_year`
--

LOCK TABLES `tbl_year` WRITE;
/*!40000 ALTER TABLE `tbl_year` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_year` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lb_first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lb_middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lb_last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Kwaye',NULL,'Kant','g.kwaye@ksoft-solutions.com','$2y$10$grc2tJlTmjDKjSf0jCBpzuM42LSFdNB18.z/iEpguohsVQCiIAOw.','tYm5hhs6sQCY5tZyG5tXieqwemcb4vV8U5Ys5KdEmo8ZDs12m7eguG3R1SKW','2017-06-22 01:51:23','2017-06-22 01:51:23'),(2,'SEUNKAM','Gabriel','Loic','gabrielkwaye2@gmail.com','$2y$10$QUArt0BkdrrmAgOks/GerOmrbrtN/1GxTfk2pTS5qnHRFVetDRCXC',NULL,'2017-06-22 02:11:13','2017-06-22 02:11:13'),(3,'kima','Gabriel','Loic','bkima@kimberts.net','$2y$10$Vnu4QuXjIb/H0GkjF/dtEOh1v/1JyArHrqJvrJMkR8C09J78BUrma','MCnhh8wYFPiaNZDUVmXfvTlf1EvVLUlQT0r0TPJmdEhoc4raFkmBJqWm7ly1','2017-06-22 02:16:01','2017-06-22 02:16:01'),(7,'Gabriel','KWAYE','Kwaye','gabykant@yahoo.com','$2y$10$ujfpdj8rBNNaHaPwBPiF9eH24JJGPeQtUbLeME8x/nHUctB9j9Lzm',NULL,'2017-06-22 04:18:33','2017-06-22 04:18:33'),(8,'Francis','KWAYE','Dzomutcha','francisdzomutcha@yahoo.fr','$2y$10$U72NVV9qyr1Y0.zWUhS3pOjmJd1HCmniH0hOFNLvMsEjJMZB..73m',NULL,'2017-06-22 04:57:53','2017-06-22 04:57:53'),(9,'Actu',NULL,'Net','g.kwaye3@ksoft-solutions.com','$2y$10$7Le7b9ZdBd6EtGq8..Oy.eSycWYhE6Wke13XdUFtkXEzUWSHtyXoC',NULL,'2017-06-22 05:02:22','2017-06-22 05:02:22'),(10,'Sis','Fdh','Killer','sis.killer@rocketmail.com','$2y$10$Vg4uKGNJ3iK5kV1vwoe6Aucj1fBZM.PUX0MKyJwUWYMXW2vlpwYxO','0jNiSIPqYBQqAUIzmRENDqTqzW17vlCPLNrZ3nqkki6bIvnfNf4e6L4RdsAY','2017-06-25 20:24:57','2017-06-25 20:24:57'),(11,'Sis','KWAYE','Killer','sis.killer44@rocketmail.com','$2y$10$m3llsAi7eq1B1s47DGJOCuxQ/U6aAeUt3SLHluvAWzE/JOiu1Gafm',NULL,'2017-06-25 22:22:20','2017-06-25 22:22:20'),(12,'Gabriel','KWAYE','Kwaye','gabykant8@yahoo.com','$2y$10$DsoazGR52L48wcz.6PQ9z.2eVmzTZX01y6cmo04lYb2NY2/LiIHCm',NULL,'2017-06-25 22:29:32','2017-06-25 22:29:32'),(13,'Gabriel','KWAYE','Kwaye','gabykantd@yahoo.com','$2y$10$4ddzQhjrb7q2c90aqv9DA.hkzoG8CeRN2JwjDH5Z8zhsF6N58AwMa',NULL,'2017-06-25 22:36:00','2017-06-25 22:36:00'),(14,'Gabriel','KWAYE','Kwaye','gabykantdd@yahoo.com','$2y$10$35035jqeVlE3TulKwyujAeltuqdXNYomG1sH1DuvzkJMZmAv.Mt4a','SroLn21ab0rC8QklTBa05NR5pBR8PgkjIu9I7Vz6n3geMuniwjQWsAAmYhIz','2017-06-25 22:37:27','2017-06-25 22:37:27'),(15,'Francis','KWAYE','Dzomutcha','francisdzomutcha23@yahoo.fr','$2y$10$SzzdFDpARwjnivnOSRfa3uz/r1GGuvYfcok0k7tJKX2mw80ZnHvne',NULL,'2017-06-25 22:41:05','2017-06-25 22:41:05'),(16,'kima','KWAYE','Kwaye','bkima36@kimberts.net','$2y$10$Bg6z/PYoWaDzdWRFCCdiB./UmJnXhspaioNANOICoOJfdeJbv0Bxq','R4nzeB9ufFJVwtdja7x8rNCPhPy9s1F0tLnPUXNnBBZD6sGwwHeJDZ4qqarp','2017-06-25 22:42:48','2017-06-25 22:42:48'),(17,'SEUNKAM','KWAYE','Loic','gabrielkwaye12@gmail.com','$2y$10$htkzgmOVaGCXpSGSjE9Mtes9hB5TP0wULSldz7UZL2Fj.Pj35yMSm','aBKe46bXJ3HZuegFCaXZEFJxFubNCJpSboyxS687xWbECw7wr8uT4L4MLZYs','2017-06-26 07:29:57','2017-06-26 07:29:57'),(18,'Gabriel Kant','KWAYE','NGUEMELIEU','g.kwaye14@yahoo.de','$2y$10$B.qL5Q18gMPFbtTra4sOYuoQtFSCvdXxyu4XOyD.JJjbLDG1CWe3K','jTyhWytFG5b31ZJLwyUG3sb4eBmgD8SlDQEjhnK3qcc2wSHDkTe504l1QQ9G','2017-06-26 07:39:21','2017-06-26 07:39:21'),(19,'Karl','KWAYE','Eton','billing@ksoft-solutions.com','$2y$10$7mVejRFyoWCIH6QpWbyGSOvXchdeja4hu1EqaW.6bObP23AShVUu.',NULL,'2017-06-26 07:40:21','2017-06-26 07:40:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-30 11:25:33
